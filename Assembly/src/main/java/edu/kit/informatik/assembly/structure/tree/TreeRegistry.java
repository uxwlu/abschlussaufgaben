package edu.kit.informatik.assembly.structure.tree;

import edu.kit.informatik.assembly.exception.MaterialException;
import edu.kit.informatik.assembly.structure.MaterialRegistry;
import edu.kit.informatik.assembly.structure.visiting.CycleDetectorVisitor;
import edu.kit.informatik.assembly.structure.visiting.NodeDetectionVisitor;
import edu.kit.informatik.assembly.util.Counted;
import edu.kit.informatik.commandsystem.util.Optional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.StringJoiner;

/**
 * A {@link MaterialRegistry} using {@link MaterialTreeNode}s.
 *
 * @author uxwlu
 * @version 1.0
 */
public class TreeRegistry implements MaterialRegistry<MaterialTreeNode> {

  private final int maxNodeCount;

  private RootNode root;

  /**
   * Creates a new tree registry.
   *
   * @param maxNodeCount the maximum amount of materials you may have in a child
   */
  public TreeRegistry(int maxNodeCount) {
    this.maxNodeCount = maxNodeCount;
    this.root = new RootNode();
  }

  @Override
  public void add(String material, Collection<Counted<String>> dependencies) {
    List<Counted<MaterialTreeNode>> nodes = new ArrayList<>();
    for (Counted<String> dependency : dependencies) {
      nodes.add(
          new Counted<>(
              MaterialTreeNode.forMaterial(dependency.getValue()),
              dependency.getAmount()
          )
      );
    }

    add(MaterialTreeNode.forMaterialGroup(nodes, material));
  }

  private void add(MaterialTreeNode material) {
    // Verify input is valid
    assertNodesAreNotTooLarge(material);
    assertNoCycle(material);

    // trade in some speed/memory for safety. We can not accidentally modify our live version
    // this way
    RootNode modifiedTree = root.deepCopy();

    injectExistingMaterials(material, modifiedTree);

    // verify there are no errors after hydrating the passed in node with the existing materials
    assertNoCycle(material);

    modifiedTree.add(material);

    modifiedTree.replaceWith(material);

    // commit changes
    this.root = modifiedTree;
  }

  private void assertNodesAreNotTooLarge(MaterialTreeNode material) {
    NodeDetectionVisitor<MaterialTreeNode> visitor = new NodeDetectionVisitor<>(
        node -> node.getAmount() > maxNodeCount
    );
    material.acceptTree(visitor);
    if (!visitor.getFound().isEmpty()) {
      StringJoiner joiner = new StringJoiner(", ");
      for (Counted<MaterialTreeNode> node : visitor.getFound()) {
        joiner.add(node.getValue().getName() + "(" + node.getAmount() + ")");
      }
      throw new MaterialException(
          "The following nodes have more than " + maxNodeCount + " elements: " + joiner
      );
    }
  }

  private void assertNoCycle(MaterialTreeNode material) {
    material.acceptLayer(new CycleDetectorVisitor<>(material));
  }

  /**
   * Replaces all materials with stored ones, if the names match. This ensures that the tree is
   * fully built, even when a shallow object is passed in
   *
   * @param material the material that should be expanded
   */
  private void injectExistingMaterials(MaterialTreeNode material, RootNode root) {
    List<MaterialTreeNode> allNodes = root.getAll();
    material.replaceOnePass(allNodes);
  }

  @Override
  public Optional<MaterialTreeNode> get(String name) {
    return root.find(name);
  }

  @Override
  public boolean removeGroup(String name) {
    boolean contained = root.find(name)
        .map(MaterialTreeNode::hasDependencies)
        .orElse(false);

    if (!contained) {
      return false;
    }

    root.removeGroup(name);

    return true;
  }

  @Override
  public void adjustPart(String root, Counted<String> modification) {
    // trade in some speed/memory for safety. We can not accidentally modify our live version
    // this way
    RootNode modifiedTree = this.root.deepCopy();
    MaterialTreeNode modifiedNode = modifiedTree.adjustPart(root, modification);

    assertNoCycle(modifiedNode);
    assertNodesAreNotTooLarge(modifiedNode);

    this.root = modifiedTree;
  }
}
