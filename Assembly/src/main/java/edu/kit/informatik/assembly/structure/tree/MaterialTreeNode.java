package edu.kit.informatik.assembly.structure.tree;

import edu.kit.informatik.assembly.exception.MaterialException;
import edu.kit.informatik.assembly.structure.Material;
import edu.kit.informatik.assembly.util.Counted;
import edu.kit.informatik.commandsystem.util.Optional;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
import java.util.Set;

/**
 * A {@link Material} represented as a tree.
 *
 * @author uxwlu
 * @version 1.0
 */
public final class MaterialTreeNode implements Material<MaterialTreeNode> {

  private final List<Counted<MaterialTreeNode>> children;
  private final String name;

  /**
   * Creates a new tree node.
   *
   * @param children the child nodes
   * @param name the name
   */
  private MaterialTreeNode(List<Counted<MaterialTreeNode>> children, String name) {
    this.children = new ArrayList<>(children);
    this.name = name;
  }

  @Override
  public List<Counted<MaterialTreeNode>> getNeededMaterials() {
    return Collections.unmodifiableList(children);
  }

  @Override
  public String getName() {
    return name;
  }

  /**
   * Creates a deep copy of this material.
   *
   * @return a deep copy of this material
   */
  MaterialTreeNode deepCopy() {
    List<Counted<MaterialTreeNode>> childrenCopy = new ArrayList<>();
    for (Counted<MaterialTreeNode> child : this.children) {
      childrenCopy.add(new Counted<>(child.getValue().deepCopy(), child.getAmount()));
    }
    return new MaterialTreeNode(childrenCopy, name);
  }

  /**
   * Returns all children, their children and so on.
   *
   * @return all children
   */
  Set<MaterialTreeNode> getChildrenRecursive() {
    Set<MaterialTreeNode> children = new HashSet<>();
    for (Counted<MaterialTreeNode> child : this.children) {
      children.add(child.getValue());
      children.addAll(child.getValue().getChildrenRecursive());
    }
    return children;
  }

  /**
   * Adds the given amount of a material. If a child with that name already exists, it adds it to
   * its count.
   *
   * <p>If the count becomes negative or zero, the element is removed.</p>
   *
   * @param node the node of the material to add
   * @param amount the amount to add
   * @throws edu.kit.informatik.assembly.exception.MaterialException if an error occurred
   */
  void add(MaterialTreeNode node, int amount) {
    ListIterator<Counted<MaterialTreeNode>> iterator = children.listIterator();
    while (iterator.hasNext()) {
      Counted<MaterialTreeNode> child = iterator.next();

      if (child.getValue().equals(node)) {
        int newAmount = child.getAmount() + amount;
        if (newAmount == 0) {
          iterator.remove();
        } else if (newAmount < 0) {
          throw new MaterialException(
              "I have " + child.getAmount() + " but you tried to modify it by " + amount
          );
        } else {
          iterator.set(new Counted<>(child.getValue(), newAmount));
        }
        return;
      }
    }

    // only create it, if the amount is positive
    if (amount <= 0) {
      throw new MaterialException("Great news! It wasn't present there in the first place.");
    }

    children.add(new Counted<>(node, amount));
  }

  /**
   * Replaces all occurrences of the nodes with the passed ones.
   *
   * @param nodes the new nodes
   */
  void replaceOnePass(List<MaterialTreeNode> nodes) {
    ListIterator<Counted<MaterialTreeNode>> iterator = children.listIterator();

    while (iterator.hasNext()) {
      Counted<MaterialTreeNode> child = iterator.next();
      boolean replaced = false;
      for (MaterialTreeNode node : nodes) {
        if (node.getName().equals(child.getValue().getName())) {
          iterator.set(new Counted<>(node, child.getAmount()));
          replaced = true;
          break;
        }
      }

      if (!replaced) {
        child.getValue().replaceOnePass(nodes);
      }
    }
  }

  /**
   * Finds a given node by its name.
   *
   * @param name the name of the node
   * @return the found node
   */
  Optional<MaterialTreeNode> find(String name) {
    if (getName().equals(name)) {
      return Optional.of(this);
    }

    for (Counted<MaterialTreeNode> child : children) {
      Optional<MaterialTreeNode> childResult = child.getValue().find(name);
      if (childResult.isPresent()) {
        return childResult;
      }
    }
    return Optional.empty();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MaterialTreeNode that = (MaterialTreeNode) o;
    return Objects.equals(name, that.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name);
  }

  @Override
  public String toString() {
    return "MaterialTreeNode{"
        + "children=" + children
        + ", name='" + name + '\''
        + '}';
  }

  /**
   * Creates a new material tree node for a material.
   *
   * @param name the name of the node
   * @return the created node
   */
  public static MaterialTreeNode forMaterial(String name) {
    return new MaterialTreeNode(Collections.emptyList(), name);
  }

  /**
   * Creates a new material tree node for a material group.
   *
   * @param children the child nodes
   * @param name the name of the node
   * @return the created node
   */
  public static MaterialTreeNode forMaterialGroup(List<Counted<MaterialTreeNode>> children,
      String name) {
    return new MaterialTreeNode(children, name);
  }
}
