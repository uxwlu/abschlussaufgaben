package edu.kit.informatik.assembly.structure.visiting;

import edu.kit.informatik.assembly.structure.Material;
import edu.kit.informatik.assembly.util.Counted;
import edu.kit.informatik.assembly.util.Predicate;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Filters all nodes to only return the ones matching a given predicate.
 *
 * @param <T> the type of the material
 * @author uxwlu
 * @version 1.0
 */
public class NodeDetectionVisitor<T extends Material<T>> implements TreeVisitor<T> {

  private final Predicate<Counted<T>> predicate;
  private final Set<Counted<T>> found;

  /**
   * Creates a new visitor.
   *
   * @param predicate the predicate to use for filtering
   */
  public NodeDetectionVisitor(Predicate<Counted<T>> predicate) {
    this.predicate = Objects.requireNonNull(predicate, "predicate can not be null!");
    this.found = new HashSet<>();
  }

  @Override
  public void visitMaterial(Counted<T> material) {
    if (predicate.test(material)) {
      found.add(material);
    }
  }

  /**
   * Returns all found materials.
   *
   * @return all found materials
   */
  public Set<Counted<T>> getFound() {
    return Collections.unmodifiableSet(found);
  }
}
