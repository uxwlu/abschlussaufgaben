package edu.kit.informatik.assembly.structure.visiting;

import edu.kit.informatik.assembly.structure.Material;
import edu.kit.informatik.assembly.util.Counted;

/**
 * A visitor that can be applied to a single layer.
 *
 * @param <T> the material
 */
// This is somewhat of a weird implementation of the pattern, as it binds it to some kind of
// recursive structure, but it is still oblivious of the actual underlying implementation.
public interface LayerVisitor<T extends Material<T>> {

  /**
   * Visits a material.
   *
   * @param material the material to visit
   */
  void visitMaterial(Counted<T> material);
}
