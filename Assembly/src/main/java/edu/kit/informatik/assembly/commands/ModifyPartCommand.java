package edu.kit.informatik.assembly.commands;

import static edu.kit.informatik.assembly.commands.types.ModifyPartType.modifyPart;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.assembly.commands.types.ModifyPartType.PartModificationSpecification;
import edu.kit.informatik.assembly.structure.Material;
import edu.kit.informatik.assembly.structure.MaterialRegistry;
import edu.kit.informatik.assembly.util.Counted;
import edu.kit.informatik.commandsystem.tree.CommandNode;
import edu.kit.informatik.commandsystem.util.Function;

/**
 * A general command that modifies a part.
 *
 * @author uxwlu
 * @version 1.0
 */
class ModifyPartCommand {


  /**
   * Modifies the part count for a given material.
   *
   * @param keyword th keyword for the command
   * @param modification the modification to apply to the amount
   * @param nameSeparator the separator char between the root assembly name and the amount
   * @param maxNodeCount the maximum amount of materials you may have in a child
   * @param <T> the type of the material
   * @param <R> the type of the registry
   * @return the command
   */
  static <T extends Material<T>, R extends MaterialRegistry<T>> CommandNode<R> getCommand(
      String keyword, Function<Integer, Integer> modification, char nameSeparator,
      int maxNodeCount) {

    return CommandNode.<R>builder(keyword)
        .withArgument("change", modifyPart(nameSeparator, maxNodeCount))
        .withCommand(commandContext -> {
          R registry = commandContext.getSource();
          PartModificationSpecification change = commandContext
              .getArgument(PartModificationSpecification.class, "change");

          Counted<String> updatedModification = new Counted<>(
              change.getModification().getValue(),
              modification.apply(change.getModification().getAmount())
          );

          registry.adjustPart(change.getRoot(), updatedModification);

          Terminal.printLine("OK");
        })
        .build();
  }
}
