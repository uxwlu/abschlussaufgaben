package edu.kit.informatik.assembly.structure.maps;

import edu.kit.informatik.assembly.exception.MaterialException;
import edu.kit.informatik.assembly.structure.Material;
import edu.kit.informatik.assembly.util.Counted;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;

/**
 * A {@link Material} for use with the map registry.
 *
 * @author uxwlu
 * @version 1.0
 */
public final class MapMaterial implements Material<MapMaterial> {

  private final MapRoot root;

  private final String name;
  private final List<Counted<String>> dependencies;

  /**
   * A material for the map registry.
   *
   * @param root the root
   * @param name the name of the material
   * @param dependencies the optional dependencies
   */
  MapMaterial(MapRoot root, String name, Collection<Counted<String>> dependencies) {
    this.root = root;
    this.name = name;
    this.dependencies = new ArrayList<>(dependencies);
  }

  @Override
  public List<Counted<MapMaterial>> getNeededMaterials() {
    List<Counted<MapMaterial>> materials = new ArrayList<>();

    for (Counted<String> counted : dependencies) {
      MapMaterial material = root.getGroup(counted.getValue())
          .orElse(new MapMaterial(root, counted.getValue(), Collections.emptyList()));

      materials.add(new Counted<>(material, counted.getAmount()));
    }

    return Collections.unmodifiableList(materials);
  }

  @Override
  public String getName() {
    return name;
  }

  /**
   * Creates a deep copy of this material belonging to a new root.
   *
   * @param root the new root
   * @return the copy
   */
  MapMaterial deepCopy(MapRoot root) {
    return new MapMaterial(root, name, dependencies);
  }

  /**
   * Adds the given material or subtracts if the counted has a negative value. If the child's count
   * is negative it is removed.
   *
   * @param material the material to add
   * @throws MaterialException if there is not enough of the material to remove
   */
  void addChild(Counted<String> material) {
    ListIterator<Counted<String>> iterator = dependencies.listIterator();

    while (iterator.hasNext()) {
      Counted<String> current = iterator.next();
      if (current.getValue().equals(material.getValue())) {
        int newAmount = current.getAmount() + material.getAmount();

        if (newAmount == 0) {
          iterator.remove();
        } else if (newAmount < 0) {
          throw new MaterialException(
              "Not enough of " + material.getValue() + " present (" + current.getAmount() + ")."
          );
        } else {
          iterator.set(new Counted<>(current.getValue(), newAmount));
        }
        return;
      }
    }

    if (material.getAmount() == 0) {
      throw new MaterialException("Can not add zero.");
    } else if (material.getAmount() < 0) {
      throw new MaterialException("Can not remove a part that does not exist.");
    }
    dependencies.add(material);
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MapMaterial that = (MapMaterial) o;
    return Objects.equals(name, that.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name);
  }

  @Override
  public String toString() {
    return "MapMaterial{"
        + "name='" + name + '\''
        + ", dependencies=" + dependencies
        + '}';
  }
}
