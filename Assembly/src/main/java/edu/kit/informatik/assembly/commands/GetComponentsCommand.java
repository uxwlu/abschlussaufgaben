package edu.kit.informatik.assembly.commands;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.assembly.structure.Material;
import edu.kit.informatik.assembly.structure.MaterialRegistry;
import edu.kit.informatik.assembly.structure.visiting.FindDependencyMaterialsVisitor;
import edu.kit.informatik.commandsystem.tree.CommandNode;

/**
 * A command that all assemblies for a given material.
 *
 * @author uxwlu
 * @version 1.0
 */
public class GetComponentsCommand {

  /**
   * Prints all assemblies for a given material.
   *
   * @param <T> the type of the material
   * @param <R> the type of the registry
   * @return the command
   */
  public static <T extends Material<T>, R extends MaterialRegistry<T>> CommandNode<R> getCommand() {
    return MaterialPrintingCommand.getCommand(
        (registry, material) -> {
          FindDependencyMaterialsVisitor<T> materialsVisitor = new FindDependencyMaterialsVisitor<>();
          material.acceptLayer(materialsVisitor);

          return materialsVisitor.getMaterialAmounts();
        },
        t -> Terminal.printError(t.getName() + " is no group."),
        "getComponents"
    );
  }
}
