package edu.kit.informatik.assembly.structure.tree;

import edu.kit.informatik.assembly.exception.MaterialException;
import edu.kit.informatik.assembly.structure.visiting.LayerVisitor;
import edu.kit.informatik.assembly.structure.visiting.TreeVisitable;
import edu.kit.informatik.assembly.structure.visiting.TreeVisitor;
import edu.kit.informatik.assembly.util.Counted;
import edu.kit.informatik.commandsystem.util.Optional;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * The root node for the {@link TreeRegistry}.
 *
 * @author uxwlu
 * @version 1.0
 */
class RootNode implements TreeVisitable<MaterialTreeNode> {

  private final Map<String, MaterialTreeNode> nodes;

  /**
   * Creates a new root node.
   */
  RootNode() {
    this(Collections.emptyMap());
  }

  /**
   * Creates a new root node.
   */
  private RootNode(Map<String, MaterialTreeNode> nodes) {
    this.nodes = new HashMap<>(nodes);
  }

  /**
   * Finds a given node.
   *
   * @param name the name of the node.
   * @return the node, if present
   */
  Optional<MaterialTreeNode> find(String name) {
    if (nodes.containsKey(name)) {
      return Optional.of(nodes.get(name));
    }
    for (MaterialTreeNode node : nodes.values()) {
      Optional<MaterialTreeNode> childResult = node.find(name);
      if (childResult.isPresent()) {
        return childResult;
      }
    }
    return Optional.empty();
  }

  /**
   * Returns all nodes in this root.
   *
   * @return all nodes
   */
  List<MaterialTreeNode> getAll() {
    List<MaterialTreeNode> nodes = new ArrayList<>();
    for (MaterialTreeNode node : this.nodes.values()) {
      nodes.add(node);
      nodes.addAll(node.getChildrenRecursive());
    }
    return nodes;
  }

  /**
   * Adds a new node.
   *
   * @param node the node to add
   * @throws MaterialException if an error occurred
   */
  void add(MaterialTreeNode node) {
    String name = node.getName();
    if (nodes.containsKey(name) && nodes.get(name).hasDependencies()) {
      throw new MaterialException("duplicate keys are not allowed (" + name + ").");
    }

    nodes.put(name, node);
  }

  /**
   * Removes a given node group.
   *
   * @param name the name of the node to remove
   */
  void removeGroup(String name) {
    nodes.remove(name);
    replaceWith(MaterialTreeNode.forMaterial(name));
  }

  /**
   * Replaces all nodes with the given name with the replacement node.
   *
   * @param replacement the replacement node
   */
  void replaceWith(MaterialTreeNode replacement) {
    if (nodes.containsKey(replacement.getName())) {
      nodes.put(replacement.getName(), replacement);
    }
    for (MaterialTreeNode node : nodes.values()) {
      node.replaceOnePass(Collections.singletonList(replacement));
    }
  }

  /**
   * Adjusts the part count.
   *
   * @param targetNodeName the name of the node to modify
   * @param modification the modification
   * @return the changed node (i.e. the target node)
   * @throws MaterialException if the node was not found or was a component
   */
  MaterialTreeNode adjustPart(String targetNodeName, Counted<String> modification) {
    Optional<MaterialTreeNode> nodeToModify = find(targetNodeName);

    if (nodeToModify.isEmpty()) {
      throw new MaterialException(targetNodeName + " not found!");
    }
    if (!nodeToModify.get().hasDependencies()) {
      throw new MaterialException(targetNodeName + " is a component.");
    }

    MaterialTreeNode toAdd = find(modification.getValue())
        .orElse(MaterialTreeNode.forMaterial(modification.getValue()));

    nodeToModify.get().add(toAdd, modification.getAmount());

    replaceWith(nodeToModify.get());

    cleanupEmptyRoots();

    return nodeToModify.get();
  }

  /**
   * Removes all assembly groups that have no dependencies anymore. This is needed as groups are
   * kept outside the tree structure for easier accessibility.
   */
  private void cleanupEmptyRoots() {
    Iterator<MaterialTreeNode> iterator = nodes.values().iterator();

    //noinspection Java8CollectionRemoveIf
    while (iterator.hasNext()) {
      MaterialTreeNode node = iterator.next();
      if (!node.hasDependencies()) {
        iterator.remove();
      }
    }
  }

  /**
   * Creates a deep copy of this node.
   *
   * @return a deep copy of this node
   */
  RootNode deepCopy() {
    Map<String, MaterialTreeNode> nodesCopy = new HashMap<>();
    for (Entry<String, MaterialTreeNode> entry : nodes.entrySet()) {
      nodesCopy.put(entry.getKey(), entry.getValue().deepCopy());
    }
    return new RootNode(nodesCopy);
  }

  @Override
  public void acceptTree(TreeVisitor<MaterialTreeNode> visitor) {
    for (MaterialTreeNode node : nodes.values()) {
      node.acceptTree(visitor);
    }
  }

  @Override
  public void acceptLayer(LayerVisitor<MaterialTreeNode> visitor) {
    for (MaterialTreeNode node : nodes.values()) {
      node.acceptLayer(visitor);
    }
  }
}
