package edu.kit.informatik.assembly.commands;

import static edu.kit.informatik.assembly.commands.types.AddAssemblyType.assemblySpecification;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.assembly.commands.types.AddAssemblyType.AssemblySpecification;
import edu.kit.informatik.assembly.structure.MaterialRegistry;
import edu.kit.informatik.commandsystem.tree.CommandNode;

/**
 * A command that allows adding assemblies.
 *
 * @author uxwlu
 * @version 1.0
 */
public class AddAssemblyCommand {

  /**
   * Returns a command that allows adding assemblies.
   *
   * @param maxNodeCount the maximum amount of materials you may have in a child
   * @param <R> the registry type
   * @return the command
   */
  public static <R extends MaterialRegistry<?>> CommandNode<R> getCommand(
      int maxNodeCount) {
    return CommandNode.<R>builder("addAssembly")
        .withArgument("specification", assemblySpecification(maxNodeCount))
        .withCommand(commandContext -> {
          R registry = commandContext.getSource();
          AssemblySpecification specification = commandContext
              .getArgument(AssemblySpecification.class, "specification");

          registry.add(specification.getName(), specification.getMaterials());

          Terminal.printLine("OK");
        })
        .build();
  }
}
