package edu.kit.informatik.assembly.structure;


import edu.kit.informatik.assembly.util.Counted;
import edu.kit.informatik.commandsystem.util.Optional;
import java.util.Collection;

/**
 * A material registry, that manages the materials.
 *
 * @param <T> the type of the material
 * @author uxwlu
 * @version 1.0
 */
public interface MaterialRegistry<T extends Material<T>> {

  /**
   * Adds a material, optionally returning an error message.
   *
   * @param material the material
   * @param dependencies the dependencies
   * @throws edu.kit.informatik.assembly.exception.MaterialException if an error occurred
   */
  void add(String material, Collection<Counted<String>> dependencies);

  /**
   * Returns a material by its name.
   *
   * @param name the name of the material
   * @return the material
   */
  Optional<T> get(String name);

  /**
   * Removes the material group with the given name by downgrading it to a single element. This
   * means it effectively clears {@link Material#getNeededMaterials()} .
   *
   * @param name the name of the material
   * @return true if it was downgraded, false if it was not contained
   */
  boolean removeGroup(String name);

  /**
   * Adjusts the material
   *
   * @param modification the modification, i.e. what should be applied
   * @param root the name of the material to modify
   * @throws edu.kit.informatik.assembly.exception.MaterialException if an error occurred, for
   *     example if it was a component
   */
  void adjustPart(String root, Counted<String> modification);
}
