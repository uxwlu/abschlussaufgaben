package edu.kit.informatik.assembly.commands;

import edu.kit.informatik.commandsystem.command.CommandResult;
import edu.kit.informatik.commandsystem.tree.CommandNode;

/**
 * A command that exists the program.
 *
 * @author uxwlu
 * @version 1.0
 */
public class QuitCommand {

  /**
   * Returns a command that exists the program.
   *
   * @param <S> the command source
   * @return the command node
   */
  public static <S> CommandNode<S> getCommand() {
    return CommandNode.<S>builder("quit")
        .withCommand(commandContext -> CommandResult.EXIT)
        .build();
  }
}
