package edu.kit.informatik.assembly.commands.types;

import edu.kit.informatik.commandsystem.arguments.StatelessArgumentType;
import edu.kit.informatik.commandsystem.exceptions.CommandSyntaxException;

/**
 * A string type that reads ascii words.
 *
 * @author uxwlu
 * @version 1.0
 */
public class CharacterStringType {

  /**
   * Reads a word consisting of a-z or A-Z until the specified end char.
   *
   * @param end the character to read until
   * @param <S> the type of the command source
   * @return the argument type
   */
  static <S> StatelessArgumentType<String, S> characterWordUntil(char end) {
    return stringReader -> {
      String read = stringReader.readUntil(end);

      if (!stringReader.canRead() || stringReader.read(1).charAt(0) != end) {
        throw new CommandSyntaxException("Expected '" + end + "'", stringReader);
      }

      if (!read.matches("[A-Za-z]+")) {
        throw new CommandSyntaxException("Expected only [A-Za-z]", stringReader);
      }

      if (read.isEmpty()) {
        throw new CommandSyntaxException(
            "Expected an argument, got an empty string.",
            stringReader
        );
      }

      return read;
    };
  }


  /**
   * Reads an argument consisting of a-z or A-Z.
   *
   * @param <S> the type of the command source
   * @return the argument type
   */
  public static <S> StatelessArgumentType<String, S> characterArgument() {
    return stringReader -> {
      String argument = stringReader.readArgument();

      if (!argument.matches("[A-Za-z]+")) {
        throw new CommandSyntaxException("Expected only [A-Za-z]", stringReader);
      }

      return argument;
    };
  }
}
