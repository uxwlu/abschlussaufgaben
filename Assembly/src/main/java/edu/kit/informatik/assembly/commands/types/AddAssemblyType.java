package edu.kit.informatik.assembly.commands.types;

import static edu.kit.informatik.assembly.commands.types.CharacterStringType.characterWordUntil;
import static edu.kit.informatik.assembly.commands.types.CountedAssemblyType.countedAssembly;

import edu.kit.informatik.assembly.util.Counted;
import edu.kit.informatik.commandsystem.arguments.ArgumentType;
import edu.kit.informatik.commandsystem.arguments.RepeatedArgumentType;
import edu.kit.informatik.commandsystem.arguments.StatelessArgumentType;
import edu.kit.informatik.commandsystem.exceptions.CommandSyntaxException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Contains types needed for adding an assembly.
 *
 * @author uxwlu
 * @version 1.0
 */
public class AddAssemblyType {

  /**
   * Returns an {@link ArgumentType} parsing a list of counted material names.
   *
   * @param maxNodeCount the maximum amount of materials you may have in a child
   * @param <S> the type of the command source
   * @return the argument type
   */
  public static <S> ArgumentType<AssemblySpecification, S> assemblySpecification(int maxNodeCount) {
    ArgumentType<List<Counted<String>>, S> assemblyParser = RepeatedArgumentType.repeated(
        1,
        Integer.MAX_VALUE,
        countedAssembly(maxNodeCount),
        ';',
        (soFar, builder) -> {
          Set<String> encountered = new HashSet<>();
          for (Counted<String> element : soFar) {
            if (!encountered.add(element.getValue())) {
              throw new CommandSyntaxException("Duplicated entry", builder.getReader());
            }
          }
        }
    );

    return builder -> {
      String name = assemblyName().parse(builder.getReader());

      return new AssemblySpecification(name, assemblyParser.parse(builder));
    };
  }

  /**
   * An argument type for the name of the assembly part.
   *
   * @param <S> the type of the command source
   * @return the argument type
   */
  private static <S> StatelessArgumentType<String, S> assemblyName() {
    return stringReader -> characterWordUntil('=').parse(stringReader);
  }

  /**
   * The specification for a new assembly.
   */
  public static final class AssemblySpecification {

    private final String name;
    private final List<Counted<String>> materials;

    /**
     * Creates a new assembly specification.
     *
     * @param name the name of it
     * @param materials the needed materials
     */
    private AssemblySpecification(String name, List<Counted<String>> materials) {
      this.name = name;
      this.materials = materials;
    }

    /**
     * Returns the name.
     *
     * @return the name
     */
    public String getName() {
      return name;
    }

    /**
     * Returns the needed materials.
     *
     * @return the needed materials
     */
    public List<Counted<String>> getMaterials() {
      return Collections.unmodifiableList(materials);
    }
  }
}
