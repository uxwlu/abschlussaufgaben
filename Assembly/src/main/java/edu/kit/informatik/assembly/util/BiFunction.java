package edu.kit.informatik.assembly.util;

/**
 * A function taking two inputs.
 *
 * @param <T> the type of the first input
 * @param <U> the type of the second input
 * @param <R> the type of the output
 * @author uxwlu
 * @version 1.0
 */
public interface BiFunction<T, U, R> {

  /**
   * Applies the function
   *
   * @param t the first argument
   * @param u the second argument
   * @return the result
   */
  R apply(T t, U u);
}
