package edu.kit.informatik.assembly.tui;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.assembly.exception.MaterialException;
import edu.kit.informatik.assembly.structure.Material;
import edu.kit.informatik.assembly.structure.MaterialRegistry;
import edu.kit.informatik.commandsystem.command.CommandExecutionResult;
import edu.kit.informatik.commandsystem.command.CommandResult;
import edu.kit.informatik.commandsystem.exceptions.CommandMiscException;
import edu.kit.informatik.commandsystem.exceptions.CommandSyntaxException;
import edu.kit.informatik.commandsystem.tree.CommandDispatcher;
import edu.kit.informatik.commandsystem.tree.CommandNode;
import java.util.Objects;

/**
 * The terminal ui.
 *
 * @param <T> the type of the material
 * @param <R> the type of the registry
 * @author uxwlu
 * @version 1.0
 */
public class Tui<T extends Material<T>, R extends MaterialRegistry<T>> {

  private static final char ARGUMENT_SEPARATOR = ';';
  private static final String COMMAND_ARGUMENT_SEPARATOR = " ";
  private final CommandDispatcher<R> commandDispatcher;
  private final R registry;

  /**
   * Creates a new tui for the given game.
   *
   * @param registry the registry to use
   */
  public Tui(R registry) {
    this.commandDispatcher = new CommandDispatcher<>(
        ARGUMENT_SEPARATOR,
        COMMAND_ARGUMENT_SEPARATOR
    );
    this.registry = Objects.requireNonNull(registry, "registry can not be null!");
  }

  /**
   * Adds a new command.
   *
   * @param node the command node
   */
  public void addCommand(CommandNode<R> node) {
    commandDispatcher.addCommand(node);
  }

  /**
   * Runs the tui.
   */
  public void run() {
    while (true) {
      try {
        String input = Terminal.readLine();
        CommandExecutionResult result = commandDispatcher.execute(registry, input);

        if (result.getResult() == CommandResult.EXIT) {
          return;
        }
      } catch (CommandSyntaxException | CommandMiscException | MaterialException e) {
        Terminal.printError(decaptialise(e.getMessage()));
      }
    }
  }

  /**
   * Decapitalises a word by making the first character lowercase.
   *
   * @param input the input string
   * @return the string with the first char in lower case
   */
  private static String decaptialise(String input) {
    return input.isEmpty() ? "" : input.substring(0, 1).toLowerCase() + input.substring(1);
  }

}
