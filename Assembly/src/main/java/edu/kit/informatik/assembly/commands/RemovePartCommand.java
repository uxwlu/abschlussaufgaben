package edu.kit.informatik.assembly.commands;

import edu.kit.informatik.assembly.structure.Material;
import edu.kit.informatik.assembly.structure.MaterialRegistry;
import edu.kit.informatik.commandsystem.tree.CommandNode;

/**
 * A command that removes a part from a group.
 *
 * @author uxwlu
 * @version 1.0
 */
public class RemovePartCommand {

  /**
   * Returns a command that removes a part from a group.
   *
   * @param maxNodeCount the maximum amount of materials you may have in a child
   * @param <T> the type of the material
   * @param <R> the type of the registry
   * @return the command
   */
  public static <T extends Material<T>, R extends MaterialRegistry<T>> CommandNode<R> getCommand(
      int maxNodeCount) {
    return ModifyPartCommand.getCommand("removePart", i -> -i, '-', maxNodeCount);
  }
}
