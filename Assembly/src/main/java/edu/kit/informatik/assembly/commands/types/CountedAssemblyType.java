package edu.kit.informatik.assembly.commands.types;

import static edu.kit.informatik.assembly.commands.types.CharacterStringType.characterArgument;
import static edu.kit.informatik.assembly.commands.types.PositiveIntegerTypes.unpaddedPositiveInRange;

import edu.kit.informatik.assembly.util.Counted;
import edu.kit.informatik.commandsystem.arguments.StatelessArgumentType;
import edu.kit.informatik.commandsystem.exceptions.CommandSyntaxException;

/**
 * Contains n {@link StatelessArgumentType} that can parse an assembly with a count.
 *
 * @author uxwlu
 * @version 1.0
 */
class CountedAssemblyType {

  /**
   * Returns a counted assembly type.
   *
   * @param maxNodeCount the maximum amount of materials you may have in a child
   * @param <S> the type of the command source
   * @return the argument type
   */
  static <S> StatelessArgumentType<Counted<String>, S> countedAssembly(int maxNodeCount) {
    return stringReader -> {
      int count = unpaddedPositiveInRange(1, maxNodeCount).parse(stringReader);

      if (!stringReader.canRead() || !stringReader.read(1).equals(":")) {
        throw new CommandSyntaxException(
            "Expected ':' separator between count and name!",
            stringReader
        );
      }

      String name = characterArgument().parse(stringReader);

      return new Counted<>(name, count);
    };
  }
}
