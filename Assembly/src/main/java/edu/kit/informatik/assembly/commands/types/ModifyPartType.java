package edu.kit.informatik.assembly.commands.types;

import static edu.kit.informatik.assembly.commands.types.CharacterStringType.characterWordUntil;
import static edu.kit.informatik.assembly.commands.types.CountedAssemblyType.countedAssembly;

import edu.kit.informatik.assembly.util.Counted;
import edu.kit.informatik.commandsystem.arguments.StatelessArgumentType;

/**
 * A type used for modifying parts.
 *
 * @author uxwlu
 * @version 1.0
 */
public class ModifyPartType {

  /**
   * An argument type that encapsulates changing a part count in a given assembly.
   *
   * @param nameSeparator the separator char between the root assembly name and the amount
   * @param maxNodeCount the maximum amount of materials you may have in a child
   * @param <S> the type of the source
   * @return the type
   */
  public static <S> StatelessArgumentType<PartModificationSpecification, S> modifyPart(
      char nameSeparator, int maxNodeCount) {
    return stringReader -> {
      String name = characterWordUntil(nameSeparator).parse(stringReader);
      Counted<String> countedAssembly = countedAssembly(maxNodeCount).parse(stringReader);

      return new PartModificationSpecification(name, countedAssembly);
    };
  }

  /**
   * The specification for modifying a part count.
   */
  public static final class PartModificationSpecification {

    private final String root;
    private final Counted<String> modification;

    private PartModificationSpecification(String root, Counted<String> modification) {
      this.root = root;
      this.modification = modification;
    }

    /**
     * Returns the root material that is being modified.
     *
     * @return the root material that is being modified
     */
    public String getRoot() {
      return root;
    }

    /**
     * Returns the modification.
     *
     * @return the modification
     */
    public Counted<String> getModification() {
      return modification;
    }
  }
}
