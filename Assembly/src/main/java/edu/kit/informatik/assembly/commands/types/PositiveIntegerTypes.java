package edu.kit.informatik.assembly.commands.types;

import edu.kit.informatik.commandsystem.arguments.StatelessArgumentType;
import edu.kit.informatik.commandsystem.exceptions.CommandSyntaxException;
import edu.kit.informatik.commandsystem.util.StringReader;
import java.util.regex.Pattern;

/**
 * Contains some integer types needed for dawn commands.
 *
 * @author uxwlu
 * @version 1.0
 */
class PositiveIntegerTypes {

  /**
   * Returns an argument parsing an un-padded positive integer in a given range.
   *
   * @param min the minimum value (inclusive)
   * @param max the maximum value (inclusive)
   * @param <S> the command source
   * @return an argument parsing an un-padded positive integer in a given range
   */
  static <S> StatelessArgumentType<Integer, S> unpaddedPositiveInRange(int min, int max) {
    return stringReader -> {
      String read = stringReader.read(Pattern.compile("\\d+"));

      int integer = parseUnpaddedInt(stringReader, read);

      if (integer < min || integer > max) {
        throw new CommandSyntaxException(
            String.format(
                "Integer out of bounds, expected %d <= x <= %d, got %d",
                min, max, integer
            ),
            stringReader
        );
      }

      return integer;
    };
  }

  private static int parseUnpaddedInt(StringReader stringReader, String input)
      throws CommandSyntaxException {
    try {
      int parsed = Integer.parseInt(input);

      if (parsed < 0) {
        throw new CommandSyntaxException("The number may not be negative!", stringReader);
      }

      if (input.matches("0\\d+")) {
        throw new CommandSyntaxException("Numbers may not have leading zeros", stringReader);
      }
      return parsed;
    } catch (NumberFormatException e) {
      throw new CommandSyntaxException("Expected an integer", stringReader);
    }
  }
}
