package edu.kit.informatik.assembly.structure.maps;

import edu.kit.informatik.assembly.exception.MaterialException;
import edu.kit.informatik.assembly.util.Counted;
import edu.kit.informatik.commandsystem.util.Optional;
import java.util.HashMap;
import java.util.Map;

/**
 * The root for the map registory.
 *
 * @author uxwlu
 * @version 1.0
 */
class MapRoot {

  private final Map<String, MapMaterial> materials;

  /**
   * Creates a new map registry.
   */
  MapRoot() {
    this.materials = new HashMap<>();
  }

  /**
   * Returns a material group, if it exists.
   *
   * @param name the name of the group
   * @return the group
   */
  Optional<MapMaterial> getGroup(String name) {
    return Optional.ofNullable(materials.get(name));
  }

  /**
   * Adds a given group.
   *
   * @param mapMaterial the group to add
   */
  void addGroup(MapMaterial mapMaterial) {
    if (materials.containsKey(mapMaterial.getName())) {
      throw new MaterialException("Material '" + mapMaterial.getName() + "' already exists.");
    }
    materials.put(mapMaterial.getName(), mapMaterial);
  }

  /**
   * Removes a group. Downgrades it to a component if it is used anywhere else.
   *
   * @param name the group name
   * @return true if the group existed
   */
  boolean removeGroup(String name) {
    return materials.remove(name) != null;
  }

  /**
   * Finds a material with the given name.
   *
   * @param name the name of the material
   * @return the material
   */
  Optional<MapMaterial> find(String name) {
    if (materials.containsKey(name)) {
      return Optional.of(materials.get(name));
    }

    for (MapMaterial material : materials.values()) {
      for (Counted<MapMaterial> neededMaterial : material.getNeededMaterials()) {
        if (neededMaterial.getValue().getName().equals(name)) {
          return Optional.of(neededMaterial.getValue());
        }
      }
    }

    return Optional.empty();
  }


  /**
   * Creates a deep copy of this root.
   *
   * @return the copy
   */
  MapRoot deepCopy() {
    MapRoot copy = new MapRoot();

    for (MapMaterial material : this.materials.values()) {
      copy.materials.put(material.getName(), material.deepCopy(copy));
    }

    return copy;
  }
}
