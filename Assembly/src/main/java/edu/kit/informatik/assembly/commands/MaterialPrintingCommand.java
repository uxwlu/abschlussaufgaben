package edu.kit.informatik.assembly.commands;

import static edu.kit.informatik.assembly.commands.types.CharacterStringType.characterArgument;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.assembly.structure.Material;
import edu.kit.informatik.assembly.structure.MaterialRegistry;
import edu.kit.informatik.assembly.util.BiFunction;
import edu.kit.informatik.assembly.util.Consumer;
import edu.kit.informatik.commandsystem.tree.CommandNode;
import edu.kit.informatik.commandsystem.util.Optional;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringJoiner;

/**
 * A command that prints materials which were extracted based on some material and registry.
 *
 * @author uxwlu
 * @version 1.0
 */
class MaterialPrintingCommand {

  /**
   * Prints all materials the function extracts.
   *
   * @param extraction the extraction function
   * @param emptyHandler the empty handler that will be called if the extraction did not yield
   *     anything
   * @param keyword the keyword for the command
   * @param <T> the type of the material
   * @param <R> the type of the registry
   * @return the command
   */
  static <T extends Material<T>, R extends MaterialRegistry<T>> CommandNode<R> getCommand(
      BiFunction<R, T, Map<String, Long>> extraction, Consumer<T> emptyHandler, String keyword) {

    return CommandNode.<R>builder(keyword)
        .withArgument("name", characterArgument())
        .withCommand(commandContext -> {
          R registry = commandContext.getSource();
          String name = commandContext.getArgument(String.class, "name");

          Optional<T> material = registry.get(name);

          if (material.isEmpty()) {
            Terminal.printError(name + " not found.");
            return;
          }

          Map<String, Long> elements = extraction.apply(registry, material.get());

          if (elements.isEmpty()) {
            emptyHandler.accept(material.get());
            return;
          }

          List<Entry<String, Long>> materials = new ArrayList<>(elements.entrySet());
          materials.sort(
              Entry.<String, Long>comparingByValue(Comparator.reverseOrder())
                  .thenComparing(Entry.comparingByKey())
          );

          StringJoiner builder = new StringJoiner(";");
          for (Entry<String, Long> entry : materials) {
            builder.add(entry.getKey() + ":" + entry.getValue());
          }

          Terminal.printLine(builder);
        })
        .build();
  }
}
