// @formatter:off
/**
 * Contains an implementation of a small material management system (MMS).
 *
 * The code is roughly split into the following parts:
 * <ul>
 *   <li>
 *     <strong>Commands:</strong>
 *     <br>A few commands are virtually identical and have been abstracted away. Apart from that
 *     the commands are fairly straightforward and operate with Materials and MaterialRegistries.
 *     This strong decoupling allows for changing out the whole material implementation without
 *     touching any commands.
 *   </li>
 *   <li>
 *     <string>Util:</string>
 *     <br>Contains a few utility methods and interfaces. Most of those are just recreations of
 *     functional interfaces.
 *   </li>
 *   <li>
 *     <string>TUI:</string>
 *     <br>The Terminal UI does not really live up to its name but allows interaction with the user
 *     via the standard in and output.
 *   </li>
 *   <li>
 *     <string>Structure / Visiting:</string>
 *     <br>As the materials are inherently recursive, common operations like counting or detecting
 *     cycles can be written for a tree-like structure by employing the visitor pattern. This
 *     decouples the algorithms from the underlying materials. In fact, it allows them to work
 *     with any arbitrary material completely unchanged.
 *   </li>
 *   <li>
 *     <string>Structure / Map:</string>
 *     <br>This is an implementation of the Material and MaterialRegistry with a map. It is a quite
 *     simple implementation based on a root that has references to all material groups.
 *
 *     <br>An alternative solution using a tree-graph hybrid was also developed to make sure the
 *     Material abstraction is not leaky. It is not included in this submission as it would have
 *     been essentially dead code.
 *   </li>
 * </ul>
 */
package edu.kit.informatik.assembly;
// @formatter:on