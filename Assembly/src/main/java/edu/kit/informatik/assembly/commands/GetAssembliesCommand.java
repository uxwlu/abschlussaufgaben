package edu.kit.informatik.assembly.commands;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.assembly.structure.Material;
import edu.kit.informatik.assembly.structure.MaterialRegistry;
import edu.kit.informatik.assembly.structure.visiting.FindDependencyGroupsVisitor;
import edu.kit.informatik.commandsystem.tree.CommandNode;

/**
 * A command that prints assemblies (but no components).
 *
 * @author uxwlu
 * @version 1.0
 */
public class GetAssembliesCommand {

  /**
   * Prints all assemblies for a given material.
   *
   * @param <T> the type of the material
   * @param <R> the type of the registry
   * @return the command
   */
  public static <T extends Material<T>, R extends MaterialRegistry<T>> CommandNode<R> getCommand() {
    return MaterialPrintingCommand.getCommand(
        (registry, material) -> {
          FindDependencyGroupsVisitor<T> groupsVisitor = new FindDependencyGroupsVisitor<>();
          material.acceptLayer(groupsVisitor);

          return groupsVisitor.getMaterialAmounts();
        },
        t -> {
          if (!t.hasDependencies()) {
            Terminal.printError(t.getName() + " is a component!");
          } else {
            Terminal.printLine("EMPTY");
          }
        },
        "getAssemblies"
    );
  }
}
