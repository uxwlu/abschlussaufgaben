package edu.kit.informatik.assembly.util;

/**
 * A consumer.
 *
 * @param <T> the type of the input
 * @author uxwlu
 * @version 1.0
 */
public interface Consumer<T> {

  /**
   * Consumes some input.
   *
   * @param t the input
   */
  void accept(T t);
}
