package edu.kit.informatik.assembly.structure.visiting;

import edu.kit.informatik.assembly.exception.MaterialException;
import edu.kit.informatik.assembly.structure.Material;
import edu.kit.informatik.assembly.util.Counted;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringJoiner;

/**
 * A {@link TreeVisitor} ensuring there are no cycles.
 *
 * @param <T> the type of the mtaerial
 * @author uxwlu
 * @version 1.0
 */
public final class CycleDetectorVisitor<T extends Material<T>> implements LayerVisitor<T> {

  private final List<T> path;

  /**
   * Creates a new cycle detector visitor.
   *
   * @param start the starting material
   */
  public CycleDetectorVisitor(T start) {
    this(Collections.singletonList(start));
  }

  private CycleDetectorVisitor(List<T> path) {
    this.path = new ArrayList<>(path);
  }

  @Override
  public void visitMaterial(Counted<T> material) {
    for (T pathEntry : path) {
      if (material.getValue().equals(pathEntry)) {
        throwFoundCycleException(pathEntry);
        return;
      }
    }

    List<T> newPath = new ArrayList<>(path);
    newPath.add(material.getValue());

    CycleDetectorVisitor<T> recursiveChild = new CycleDetectorVisitor<>(newPath);
    material.getValue().acceptLayer(recursiveChild);
  }

  private void throwFoundCycleException(T material) {
    List<T> detectedCycle = new ArrayList<>(path);
    detectedCycle.add(material);

    StringJoiner message = new StringJoiner("->");
    for (T node : detectedCycle) {
      message.add(node.getName());
    }

    throw new MaterialException("found a cycle: " + message);
  }
}
