package edu.kit.informatik.assembly.tui;

import edu.kit.informatik.assembly.commands.AddAssemblyCommand;
import edu.kit.informatik.assembly.commands.AddPartCommand;
import edu.kit.informatik.assembly.commands.GetAssembliesCommand;
import edu.kit.informatik.assembly.commands.GetComponentsCommand;
import edu.kit.informatik.assembly.commands.PrintAssemblyCommand;
import edu.kit.informatik.assembly.commands.QuitCommand;
import edu.kit.informatik.assembly.commands.RemoveAssemblyCommand;
import edu.kit.informatik.assembly.commands.RemovePartCommand;
import edu.kit.informatik.assembly.structure.maps.MapMaterial;
import edu.kit.informatik.assembly.structure.maps.MapRegistry;

/**
 * The tui main class.
 *
 * @author uxwlu
 * @version 1.0
 */
public class TuiMain {

  private static final int MAX_NODE_COUNT = 1000;

  /**
   * The main method.
   *
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    MapRegistry registry = new MapRegistry(MAX_NODE_COUNT);
    Tui<MapMaterial, MapRegistry> tui = new Tui<>(registry);

    tui.addCommand(QuitCommand.getCommand());
    tui.addCommand(AddAssemblyCommand.getCommand(MAX_NODE_COUNT));
    tui.addCommand(PrintAssemblyCommand.getCommand());
    tui.addCommand(GetComponentsCommand.getCommand());
    tui.addCommand(GetAssembliesCommand.getCommand());
    tui.addCommand(RemoveAssemblyCommand.getCommand());
    tui.addCommand(RemovePartCommand.getCommand(MAX_NODE_COUNT));
    tui.addCommand(AddPartCommand.getCommand(MAX_NODE_COUNT));

    tui.run();
  }
}
