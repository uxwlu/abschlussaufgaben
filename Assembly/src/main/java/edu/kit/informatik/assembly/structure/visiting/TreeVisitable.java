package edu.kit.informatik.assembly.structure.visiting;

import edu.kit.informatik.assembly.structure.Material;

/**
 * An object that has some kind of tree-like structure and can accept a visitor.
 *
 * @param <T> the type of the material
 * @author uxwlu
 * @version 1.0
 */
public interface TreeVisitable<T extends Material<T>> {

  /**
   * Applies the visitor to all children of this object and then traverses down.
   *
   * @param visitor the visitor
   */
  void acceptTree(TreeVisitor<T> visitor);

  /**
   * Applies the visitor to all children of this object, but nothing more.
   *
   * @param visitor the visitor
   */
  void acceptLayer(LayerVisitor<T> visitor);
}
