package edu.kit.informatik.assembly.exception;

/**
 * The material exception.
 *
 * @author uxwlu
 * @version 1.0
 */
public class MaterialException extends RuntimeException {

  /**
   * Creates a new material exception with the given message.
   *
   * @param message the message
   */
  public MaterialException(String message) {
    super(message);
  }
}
