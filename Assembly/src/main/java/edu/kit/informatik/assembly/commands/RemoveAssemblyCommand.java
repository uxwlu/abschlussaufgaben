package edu.kit.informatik.assembly.commands;

import static edu.kit.informatik.assembly.commands.types.CharacterStringType.characterArgument;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.assembly.structure.MaterialRegistry;
import edu.kit.informatik.commandsystem.tree.CommandNode;

/**
 * A command that removes an assembly group.
 *
 * @author uxwlu
 * @version 1.0
 */
public class RemoveAssemblyCommand {

  /**
   * Returns a command removing an assembly group.
   *
   * @param <R> the type of the registry
   * @return the command
   */
  public static <R extends MaterialRegistry<?>> CommandNode<R> getCommand() {
    return CommandNode.<R>builder("removeAssembly")
        .withArgument("name", characterArgument())
        .withCommand(commandContext -> {
          R registry = commandContext.getSource();
          String name = commandContext.getArgument(String.class, "name");

          if (registry.removeGroup(name)) {
            Terminal.printLine("OK");
          } else {
            Terminal.printError(name + " was not found.");
          }
        })
        .build();
  }
}
