package edu.kit.informatik.assembly.structure.visiting;

import edu.kit.informatik.assembly.structure.Material;
import java.util.Map;

/**
 * A visitor collecting dependency information for a material.
 *
 * @param <T> the type of the material
 * @author uxwlu
 * @version 1.0
 */
public class FindDependencyGroupsVisitor<T extends Material<T>> extends CountingVisitor<T> {

  /**
   * Creates a new visitor collecting dependency information.
   */
  public FindDependencyGroupsVisitor() {
    super(material -> material.getValue().hasDependencies());
  }

  @Override
  public Map<String, Long> getMaterialAmounts() {
    return super.getMaterialAmounts();
  }
}
