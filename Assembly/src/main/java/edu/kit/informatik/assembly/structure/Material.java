package edu.kit.informatik.assembly.structure;

import edu.kit.informatik.assembly.structure.visiting.LayerVisitor;
import edu.kit.informatik.assembly.structure.visiting.TreeVisitable;
import edu.kit.informatik.assembly.structure.visiting.TreeVisitor;
import edu.kit.informatik.assembly.util.Counted;
import java.util.List;

/**
 * A single material.
 *
 * @param <T> the type of the material
 * @author uxwlu
 * @version 1.0
 */
public interface Material<T extends Material<T>> extends TreeVisitable<T> {

  /**
   * Returns all materials needed for this one.
   *
   * @return all materials needed for this one
   */
  List<Counted<T>> getNeededMaterials();

  /**
   * Returns the name of this material.
   *
   * @return the name of the material
   */
  String getName();

  /**
   * Returns whether this material has dependencies.
   *
   * @return true if this material has dependencies
   */
  default boolean hasDependencies() {
    return !getNeededMaterials().isEmpty();
  }

  /**
   * Applies the visitor to all children of this material and then traverses down.
   *
   * @param visitor the visitor
   */
  default void acceptTree(TreeVisitor<T> visitor) {
    for (Counted<T> neededMaterial : getNeededMaterials()) {
      visitor.visitMaterial(neededMaterial);
      neededMaterial.getValue().acceptTree(visitor);
    }
  }

  /**
   * Applies the visitor to all children of this material, but nothing more.
   *
   * @param visitor the visitor
   */
  default void acceptLayer(LayerVisitor<T> visitor) {
    for (Counted<T> neededMaterial : getNeededMaterials()) {
      visitor.visitMaterial(neededMaterial);
    }
  }
}
