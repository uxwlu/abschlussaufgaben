package edu.kit.informatik.assembly.structure.visiting;

import edu.kit.informatik.assembly.structure.Material;
import edu.kit.informatik.assembly.util.Counted;
import edu.kit.informatik.assembly.util.Predicate;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

/**
 * A visitor that counts something about nodes and relates it to them.
 *
 * @param <T> the material
 * @author uxwlu
 * @version 1.0
 */
class CountingVisitor<T extends Material<T>> implements LayerVisitor<T> {

  private final int multiplier;
  private final Map<String, Long> materialAmounts;
  private final Predicate<Counted<T>> filter;

  /**
   * Creates a new visitor collecting dependency information.
   *
   * @param filter the function to filter which nodes to count
   */
  CountingVisitor(Predicate<Counted<T>> filter) {
    this(1, filter);
  }

  /**
   * Creates a new visitor collecting dependency information.
   *
   * @param multiplier the multiplier
   * @param filter the function to filter which nodes to count
   */
  private CountingVisitor(int multiplier, Predicate<Counted<T>> filter) {
    this.multiplier = multiplier;
    this.filter = Objects.requireNonNull(filter, "filter can not be null!");
    this.materialAmounts = new HashMap<>();
  }

  @Override
  public void visitMaterial(Counted<T> material) {
    String name = material.getValue().getName();

    if (filter.test(material)) {
      addValue(name, material.getAmount());
    }

    if (material.getValue().hasDependencies()) {
      CountingVisitor<T> visitor = new CountingVisitor<>(material.getAmount(), filter);
      material.getValue().acceptLayer(visitor);

      for (Entry<String, Long> entry : visitor.getMaterialAmounts().entrySet()) {
        addValue(entry.getKey(), entry.getValue());
      }
    }
  }

  private void addValue(String name, long amount) {
    long count = materialAmounts.getOrDefault(name, 0L);
    materialAmounts.put(name, count + multiplier * amount);
  }

  /**
   * Returns the collected amounts.
   *
   * @return the collected amounts
   */
  Map<String, Long> getMaterialAmounts() {
    return Collections.unmodifiableMap(materialAmounts);
  }
}
