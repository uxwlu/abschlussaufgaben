package edu.kit.informatik.assembly.structure.maps;

import edu.kit.informatik.assembly.exception.MaterialException;
import edu.kit.informatik.assembly.structure.MaterialRegistry;
import edu.kit.informatik.assembly.structure.visiting.CycleDetectorVisitor;
import edu.kit.informatik.assembly.structure.visiting.NodeDetectionVisitor;
import edu.kit.informatik.assembly.util.Counted;
import edu.kit.informatik.commandsystem.util.Optional;
import java.util.Collection;
import java.util.StringJoiner;

/**
 * A {@link MaterialRegistry} using a map.
 *
 * @author uxwlu
 * @version 1.0
 */
public class MapRegistry implements MaterialRegistry<MapMaterial> {

  private final int maxNodeCount;

  private MapRoot root;

  /**
   * Creates a new map registry.
   *
   * @param maxNodeCount the maximum node count
   */
  public MapRegistry(int maxNodeCount) {
    this.maxNodeCount = maxNodeCount;

    this.root = new MapRoot();
  }

  @Override
  public void add(String material, Collection<Counted<String>> dependencies) {
    if (dependencies.isEmpty()) {
      throw new MaterialException("I can only add a group this way!");
    }

    MapRoot copy = root.deepCopy();
    MapMaterial mapMaterial = new MapMaterial(copy, material, dependencies);

    copy.addGroup(mapMaterial);

    assertValid(mapMaterial);

    this.root = copy;
  }

  @Override
  public Optional<MapMaterial> get(String name) {
    return root.find(name);
  }

  @Override
  public boolean removeGroup(String name) {
    return root.removeGroup(name);
  }

  @Override
  public void adjustPart(String root, Counted<String> modification) {
    MapRoot copy = this.root.deepCopy();
    Optional<MapMaterial> material = copy.find(root);

    if (!material.isPresent()) {
      throw new MaterialException("Material '" + root + "' not found!");
    }
    if (!material.get().hasDependencies()) {
      throw new MaterialException("Material '" + root + "' is a component!");
    }
    material.get().addChild(modification);

    assertValid(material.get());

    if (!material.get().hasDependencies()) {
      copy.removeGroup(root);
    }

    this.root = copy;
  }

  private void assertValid(MapMaterial material) {
    assertNoCycle(material);
    assertNodesAreNotTooLarge(material);
  }

  private void assertNodesAreNotTooLarge(MapMaterial material) {
    NodeDetectionVisitor<MapMaterial> visitor = new NodeDetectionVisitor<>(
        node -> node.getAmount() > maxNodeCount
    );
    material.acceptTree(visitor);
    if (!visitor.getFound().isEmpty()) {
      StringJoiner joiner = new StringJoiner(", ");
      for (Counted<MapMaterial> node : visitor.getFound()) {
        joiner.add(node.getValue().getName() + "(" + node.getAmount() + ")");
      }
      throw new MaterialException(
          "The following nodes have more than " + maxNodeCount + " elements: " + joiner
      );
    }
  }

  private void assertNoCycle(MapMaterial material) {
    material.acceptLayer(new CycleDetectorVisitor<>(material));
  }
}
