package edu.kit.informatik.assembly.commands;

import static edu.kit.informatik.assembly.commands.types.CharacterStringType.characterArgument;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.assembly.structure.Material;
import edu.kit.informatik.assembly.structure.MaterialRegistry;
import edu.kit.informatik.assembly.util.Counted;
import edu.kit.informatik.commandsystem.exceptions.CommandMiscException;
import edu.kit.informatik.commandsystem.tree.CommandNode;
import edu.kit.informatik.commandsystem.util.Optional;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

/**
 * A command that prints an assembly.
 *
 * @author uxwlu
 * @version 1.0
 */
public class PrintAssemblyCommand {

  /**
   * Returns a command that prints an assembly.
   *
   * @param <T> the type of the material
   * @param <R> the type of the registry
   * @return the command
   */
  public static <T extends Material<T>, R extends MaterialRegistry<T>> CommandNode<R> getCommand() {
    return CommandNode.<R>builder("printAssembly")
        .withArgument("name", characterArgument())
        .withCommand(commandContext -> {
          R registry = commandContext.getSource();
          String name = commandContext.getArgument(String.class, "name");

          Optional<T> material = registry.get(name);

          if (material.isEmpty()) {
            throw new CommandMiscException(
                "No BOM or component exists in the system for the specified name: '"
                    + name + "'"
            );
          }

          if (!material.get().hasDependencies()) {
            Terminal.printLine("COMPONENT");
            return;
          }

          List<Counted<T>> directlyNeededMaterials = new ArrayList<>(
              material.get().getNeededMaterials()
          );

          directlyNeededMaterials
              .sort((o1, o2) -> o1.getValue().getName().compareTo(o2.getValue().getName()));

          StringJoiner resultBuilder = new StringJoiner(";");
          for (Counted<T> neededMaterial : directlyNeededMaterials) {
            resultBuilder
                .add(neededMaterial.getValue().getName() + ":" + neededMaterial.getAmount());
          }

          Terminal.printLine(resultBuilder.toString());
        })
        .build();
  }
}
