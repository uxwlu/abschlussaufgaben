package edu.kit.informatik.assembly.util;

/**
 * A predicate.
 *
 * @param <T> the type
 * @author uxwlu
 * @version 1.0
 */
public interface Predicate<T> {

  /**
   * Tests whether the input fulfills the predicate.
   *
   * @param t the input
   * @return true if the input fulfills the predicate
   */
  boolean test(T t);
}
