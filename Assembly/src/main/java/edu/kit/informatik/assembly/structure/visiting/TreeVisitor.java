package edu.kit.informatik.assembly.structure.visiting;

import edu.kit.informatik.assembly.structure.Material;
import edu.kit.informatik.assembly.util.Counted;

/**
 * A visitor that can visit a whole tree at a time.
 *
 * @param <T> the type of the material
 */
// this is more general than the LayerVisitor, but as it is bound to a recursive structure (Material)
// the name tree is probably fitting -- even though you could also use Maps or whatever for the
// same effect
public interface TreeVisitor<T extends Material<T>> {

  /**
   * Visits a material.
   *
   * @param material the material to visit
   */
  void visitMaterial(Counted<T> material);
}
