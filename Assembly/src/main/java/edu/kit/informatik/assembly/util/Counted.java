package edu.kit.informatik.assembly.util;

import java.util.Objects;

/**
 * A class containing some value and an amount.
 *
 * @param <T> the type of the value
 * @author uxwlu
 * @version 1.0
 */
public final class Counted<T> {

  private final T value;
  private final int amount;

  /**
   * Creates a new counted class.
   *
   * @param value the value
   * @param amount the amount
   */
  public Counted(T value, int amount) {
    this.value = value;
    this.amount = amount;
  }

  /**
   * Returns the stored value.
   *
   * @return the stored value
   */
  public T getValue() {
    return value;
  }

  /**
   * Returns the amount.
   *
   * @return the amount
   */
  public int getAmount() {
    return amount;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Counted<?> counted = (Counted<?>) o;
    return amount == counted.amount
        && Objects.equals(value, counted.value);
  }

  @Override
  public int hashCode() {
    return Objects.hash(value, amount);
  }

  @Override
  public String toString() {
    return value + ":" + amount;
  }
}
