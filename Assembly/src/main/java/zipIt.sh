cd ~/Uni/Karlsruhe/Programmieren/Uebungsblaetter/Bearbeitung/Blatt04/CommandSystem/src/main/java
zip -r Abgabe.zip ./edu
mv Abgabe.zip ~/Uni/Karlsruhe/Programmieren/Abschlussaufgaben/Assembly/src/main/java
cd ~/Uni/Karlsruhe/Programmieren/Abschlussaufgaben/Assembly/src/main/java
zip -r Abgabe.zip ./edu
zip -d Abgabe.zip "edu/kit/informatik/Terminal.java"
zip -d Abgabe.zip "edu/kit/informatik/commandsystem/arguments/OptionalArgumentType.java"
zip -d Abgabe.zip "edu/kit/informatik/commandsystem/arguments/StringArgumentType.java"
zip -d Abgabe.zip "edu/kit/informatik/commandsystem/arguments/IntegerArgumentType.java"

zip -d Abgabe.zip "edu/kit/informatik/assembly/structure/tree"
zip -d Abgabe.zip "edu/kit/informatik/assembly/structure/tree/TreeRegistry.java"
zip -d Abgabe.zip "edu/kit/informatik/assembly/structure/tree/MaterialTreeNode.java"
zip -d Abgabe.zip "edu/kit/informatik/assembly/structure/tree/RootNode.java"
