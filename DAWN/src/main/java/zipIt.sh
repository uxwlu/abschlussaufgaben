cd ~/Uni/Karlsruhe/Programmieren/Uebungsblaetter/Bearbeitung/Blatt04/CommandSystem/src/main/java
zip -r Abgabe.zip ./edu
mv Abgabe.zip ~/Uni/Karlsruhe/Programmieren/Abschlussaufgaben/DAWN/src/main/java
cd ~/Uni/Karlsruhe/Programmieren/Abschlussaufgaben/DAWN/src/main/java
zip -r Abgabe.zip ./edu
zip -d Abgabe.zip "edu/kit/informatik/Terminal.java"
zip -d Abgabe.zip "edu/kit/informatik/commandsystem/arguments/OptionalArgumentType.java"
zip -d Abgabe.zip "edu/kit/informatik/commandsystem/arguments/StringArgumentType.java"
zip -d Abgabe.zip "edu/kit/informatik/commandsystem/arguments/IntegerArgumentType.java"
zip -d Abgabe.zip "edu/kit/informatik/dawn/command/commands/FancyRenderer.java"

# zip -d Abgabe.zip "edu/kit/informatik/dawn/package-info.java"
# zip -d Abgabe.zip "edu/kit/informatik/commandsystem/package-info.java"
