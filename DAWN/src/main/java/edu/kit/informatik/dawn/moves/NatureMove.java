package edu.kit.informatik.dawn.moves;

import edu.kit.informatik.dawn.board.Board;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.game.phases.end.EndPhase;
import edu.kit.informatik.dawn.game.phases.natureplace.NaturePlacePhase;
import edu.kit.informatik.dawn.game.phases.natureroll.NatureDecidingMovePhase;
import edu.kit.informatik.dawn.token.NatureToken;
import edu.kit.informatik.dawn.util.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Nature moved a nature token.
 *
 * @author uxwlu
 * @version 1.0
 */
public interface NatureMove extends Move {

  /**
   * Returns the token.
   *
   * @return the token
   */
  NatureToken getToken();

  /**
   * Returns the path. Starts with the token position.
   *
   * @return the path
   */
  List<Point> getPath();

  /**
   * Checks whether the path is empty.
   *
   * @return true if the path is empty
   */
  default boolean isEmpty() {
    // contains only the start position
    return getPath().size() == 1;
  }

  /**
   * Returns the end point.
   *
   * @return the end point
   */
  default Point getEnd() {
    return getPath().get(getPath().size() - 1);
  }

  /**
   * Returns the start point.
   *
   * @return the start point
   */
  default Point getStart() {
    return getPath().get(0);
  }

  /**
   * Creates a new nature move.
   *
   * @param token the token
   * @param path the path
   * @return the created move
   */
  static NatureMove of(NatureToken token, List<Point> path) {
    ArrayList<Point> pathCopy = new ArrayList<>(path);

    return new NatureMove() {
      @Override
      public NatureToken getToken() {
        return token;
      }

      @Override
      public List<Point> getPath() {
        return Collections.unmodifiableList(pathCopy);
      }

      @Override
      public void apply(Game game) {
        if (!isEmpty()) {
          Board gameBoard = game.getGameBoard();
          gameBoard.set(getStart(), null);
          gameBoard.set(getEnd(), token);
          token.setPosition(getEnd());
        }

        // mission control has more to place
        if (!game.getMissionControl().getTokens().isEmpty()) {
          game.switchPhase(new NatureDecidingMovePhase(game.getDie()));
          return;
        }

        // prepare to initiate next big phase
        game.getNature().getTokens().tokenPlaced(token);
        game.getMissionControl().reset();

        // nature also has no tokens anymore => Everything was placed, the game has ended
        if (game.getNature().getTokens().isEmpty()) {
          game.switchPhase(new EndPhase());
          return;
        }

        // back to square one, but with a new token this time
        game.switchPhase(new NaturePlacePhase(game.getNature().getCurrentToken()));
      }
    };
  }
}
