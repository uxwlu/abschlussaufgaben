package edu.kit.informatik.dawn.command.commands;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.commandsystem.tree.CommandNode;
import edu.kit.informatik.dawn.game.Game;

/**
 * A command to reset the game.
 *
 * @author uxwlu
 * @version 1.0
 */
public class ResetCommand {

  /**
   * Resets the game to its starting state.
   *
   * @return the command
   */
  public static CommandNode<Game> getCommand() {
    return CommandNode.<Game>builder("reset")
        .withCommand(commandContext -> {
          Game game = commandContext.getSource();

          game.reset();

          Terminal.printLine("OK");
        })
        .build();
  }
}
