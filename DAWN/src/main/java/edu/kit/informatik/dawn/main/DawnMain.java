package edu.kit.informatik.dawn.main;

import edu.kit.informatik.dawn.board.Board;
import edu.kit.informatik.dawn.command.commands.PrintCommand;
import edu.kit.informatik.dawn.command.commands.QuitCommand;
import edu.kit.informatik.dawn.command.commands.ResetCommand;
import edu.kit.informatik.dawn.command.commands.StateCommand;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.game.PhaseType;
import edu.kit.informatik.dawn.game.phases.end.ShowResultCommand;
import edu.kit.informatik.dawn.game.phases.missionplace.MissionControlPlaceCommand;
import edu.kit.informatik.dawn.game.phases.naturemove.NatureMoveCommand;
import edu.kit.informatik.dawn.game.phases.natureplace.NaturePlaceCommand;
import edu.kit.informatik.dawn.game.phases.natureplace.NaturePlacePhase;
import edu.kit.informatik.dawn.game.phases.natureroll.NatureRollCommand;
import edu.kit.informatik.dawn.player.MissionControl;
import edu.kit.informatik.dawn.player.Nature;
import edu.kit.informatik.dawn.token.MissionControlToken;
import edu.kit.informatik.dawn.token.MissionControlToken.MissionControlTokenType;
import edu.kit.informatik.dawn.token.NatureToken;
import edu.kit.informatik.dawn.token.NatureToken.NatureTokenType;
import edu.kit.informatik.dawn.tui.Tui;
import edu.kit.informatik.dawn.util.Die;
import edu.kit.informatik.dawn.util.Point;
import edu.kit.informatik.dawn.util.Rectangle;
import edu.kit.informatik.dawn.util.TokenDie;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The main class.
 *
 * @author uxwlu
 * @version 1.0
 */
public class DawnMain {

  private static final Rectangle BOARD_BOUNDS = Rectangle
      .spanning(new Point(0, 0), new Point(14, 10));

  private static final int MISSION_TOKEN_MIN = 2;
  private static final int MISSION_TOKEN_MAX = 6;
  private static final int DAWN_LENGTH = 7;

  /**
   * The program main.
   *
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    Nature nature = new Nature(
        Arrays.asList(
            // manually created, as I do not want to rely on the enum order
            new NatureToken(NatureTokenType.VESTA),
            new NatureToken(NatureTokenType.CERES)
        )
    );

    List<MissionControlToken> controlTokens = getMissionControlTokens();
    Die<Integer> die = new TokenDie(controlTokens);

    Game game = Game.builder()
        .withBoard(new Board(BOARD_BOUNDS))
        .withNature(nature)
        .withMissionControl(new MissionControl(controlTokens))
        .withDie(die)

        .withInitialPhase(() -> new NaturePlacePhase(nature.getCurrentToken()))

        .withPhaseCommand(PhaseType.PLACING_NATURE, NaturePlaceCommand.getCommand())
        .withPhaseCommand(
            PhaseType.DECIDING_MISSION_CONTROL_TOKEN, NatureRollCommand.getCommand(die)
        )
        .withPhaseCommand(
            PhaseType.PLACING_MISSION_CONTROL, MissionControlPlaceCommand.getCommand()
        )
        .withPhaseCommand(PhaseType.NATURE_MOVES, NatureMoveCommand.getCommand())
        .withPhaseCommand(PhaseType.GAME_ENDED, ShowResultCommand.getCommand())

        .withGameCommand(PrintCommand.getCommand())
        .withGameCommand(StateCommand.getCommand())
        .withGameCommand(QuitCommand.getCommand())
        .withGameCommand(ResetCommand.getCommand())

        .build();
    new Tui(game).run();
  }

  private static List<MissionControlToken> getMissionControlTokens() {
    List<MissionControlToken> tokens = new ArrayList<>();

    for (int i = MISSION_TOKEN_MIN; i <= MISSION_TOKEN_MAX; i++) {
      tokens.add(new MissionControlToken(i, MissionControlTokenType.NORMAL));
    }

    tokens.add(new MissionControlToken(DAWN_LENGTH, MissionControlTokenType.DAWN));

    return tokens;
  }
}
