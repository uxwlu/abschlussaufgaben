package edu.kit.informatik.dawn.game.phases.naturemove;

import static edu.kit.informatik.dawn.command.types.NaturePathType.naturePath;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.commandsystem.exceptions.CommandMiscException;
import edu.kit.informatik.commandsystem.tree.CommandNode;
import edu.kit.informatik.commandsystem.util.Optional;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.moves.NatureMove;
import edu.kit.informatik.dawn.token.NatureToken;
import edu.kit.informatik.dawn.util.Point;
import java.util.ArrayList;
import java.util.List;

/**
 * The command allowing nature to move a nature token.
 *
 * @author uxwlu
 * @version 1.0
 */
public class NatureMoveCommand {

  /**
   * Returns a command that allows nature to move.
   *
   * @return a command that allows nature to move
   */
  public static CommandNode<Game> getCommand() {
    return CommandNode.<Game>builder("move")
        .withArgument("path", naturePath(Integer.MAX_VALUE))
        .withCommand(commandContext -> {
          List<Point> points = commandContext.getList(Point.class, "path");
          Game game = commandContext.getSource();

          NatureToken sourceToken = game.getNature().getCurrentToken();

          if (!sourceToken.getPosition().isPresent()) {
            throw new CommandMiscException("The token is not on the board?!");
          }

          List<Point> path = new ArrayList<>(points);
          path.add(0, sourceToken.getPosition().get());

          NatureMove move = NatureMove.of(sourceToken, path);
          Optional<String> error = game.applyMove(move);

          if (error.isPresent()) {
            Terminal.printError(error.get());
          } else {
            Terminal.printLine("OK");
          }
        })
        .build();
  }
}
