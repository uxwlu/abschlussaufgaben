package edu.kit.informatik.dawn.game.phases.natureroll;

import edu.kit.informatik.commandsystem.util.Optional;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.moves.NatureRollMove;
import edu.kit.informatik.dawn.rules.MoveRule;
import edu.kit.informatik.dawn.util.Die;

/**
 * Enforces that the rolled value is on the die.
 *
 * @author uxwlu
 * @version 1.0
 */
class RolledSymbolOnDieRule implements MoveRule<NatureRollMove> {

  private final Die<Integer> die;

  /**
   * Creates a new rule.
   *
   * @param die the die to use
   */
  RolledSymbolOnDieRule(Die<Integer> die) {
    this.die = die;
  }

  @Override
  public Class<NatureRollMove> getMoveClass() {
    return NatureRollMove.class;
  }

  @Override
  public Optional<String> validate(NatureRollMove move, Game game) {
    if (!die.containsValue(move.getRolledValue())) {
      return Optional.of(String.format("The value %d is not on the die!", move.getRolledValue()));
    }
    return Optional.empty();
  }
}
