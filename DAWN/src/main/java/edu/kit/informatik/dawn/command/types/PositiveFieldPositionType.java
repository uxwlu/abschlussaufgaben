package edu.kit.informatik.dawn.command.types;

import static edu.kit.informatik.dawn.command.types.DawnIntegerTypes.unpaddedPositiveInRange;

import edu.kit.informatik.commandsystem.arguments.ArgumentType;
import edu.kit.informatik.commandsystem.exceptions.CommandSyntaxException;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.util.Point;
import edu.kit.informatik.dawn.util.Rectangle;

/**
 * An {@link ArgumentType} for a nature position.
 *
 * @author uxwlu
 * @version 1.0
 */
public class PositiveFieldPositionType {

  /**
   * Parses a position consisting entirely of positive integers.
   *
   * @return an argument type parsing a position for a nature token
   */
  public static ArgumentType<Point, Game> positivePosition() {
    return builder -> {
      Rectangle bounds = builder.getSource().getGameBoard().getBounds();

      int minY = bounds.getTopLeft().getY();
      int maxY = bounds.getBottomRight().getY();

      // inclusive, so subtract one
      int y = unpaddedPositiveInRange(minY, maxY)
          .parse(builder.getReader());

      if (!builder.getReader().read(1).equals(";")) {
        throw new CommandSyntaxException("Expected the separator ';'");
      }

      int minX = bounds.getTopLeft().getX();
      int maxX = bounds.getBottomRight().getX();

      // inclusive, so subtract one
      int x = unpaddedPositiveInRange(minX, maxX)
          .parse(builder.getReader());

      return new Point(x, y);
    };
  }
}
