// @formatter:off
/**
 * Contains a small implementation of <a href="https://althofer.de/dawn-11-15.html">DAWN 11/15</a>.
 *
 * <p><br>
 * The game is roughly organized into the following parts:<br>
 *
 * <ul>
 *   <li>
 *     <em>Moves:</em><br>
 *     A Move is an interface with a single method, {@link edu.kit.informatik.dawn.moves.Move#apply apply(Game)}.
 *     If the player forms any action, the action is encapsulated into a move and then, after
 *     checking whether it is valid, applied to the game. As move is an interface this does not
 *     introduce any tight coupling.
 *     <br>Mose moves have a static factory method creating an instance of them. This is provided
 *     for convenience and could be swapped out by your commands, if needed.
 *   </li>
 *   <li>
 *     <em>Rules:</em><br>
 *     Every game needs some rules and by extension something to manage them. This implementation
 *     promotes rules to first-class members, giving them their own interface. The interface only
 *     enforces two things. A method allowing you to check whether a move violates the rule and one
 *     that returns the class of moves it can handle. This is quite useful, as some rules do not make
 *     sense for any arbitrary move but are specialized. Returning the class (or interface) they
 *     require allows you to decouple the rules a lot while still making them easy to use and extend.
 *
 *     <br>Rules are collected in a RuleSet. This is just a small abstraction to ease managing them
 *     as rules are specific to phases.
 *   </li>
 *   <li>
 *     <em>Phases:</em><br>
 *     The game consists of different phases which you can also find in the code structure. A phase
 *     introduces a specific set of rules that apply to it and you can attach commands to each phase.
 *     In order to change the phase, you just ask the game nicely. The phase system splits the quite
 *     large and variable command and rule handling of the game into a few easy to use and reusable
 *     chunks.
 *   </li>
 *   <li>
 *     <em>Commands:</em><br>
 *     Next to the relevant phases (or in {@code "command.commands"} for global ones) you will find
 *     the command implementations. Commands are stored in a CommandCollection, which is phase-aware.
 *     This means that you can bind commands to individual phases (or all of them) to force the user
 *     to only use applicable commands. The command collection will notify a listener when its commands
 *     change so you can update the CommandDispatcher or whatever else you use to allow the user to
 *     execute commands.
 *
 *     <br>You will also find a few argument types that partly parse the user input in the commands
 *     package. These just make specifying commands easier and allow much quicker and more precise
 *     feedback.
 *   </li>
 *   <li>
 *     <em>Utils:</em><br>
 *     The util package contains no real surprises: A few reimplementations of functional interfaces,
 *     points and rectangles and a StreamSupplement class. The latter one is probably the most
 *     interesting, as it provides quite a few things:
 *     <br>Mapping, filtering and transforming are somewhat mixed together into a single class.
 *     While this sounds like a somewhat questionable design decision, it is mirrored in many libraries.
 *     Java provides things like that in the Math class, many static methods in interfaces (as of java 8),
 *     and especially in the Stream interface. The methods in the class are conceptually quite close.
 *     Most of what the class does is nicely solved by lazy streams, but an eager iterable mutation
 *     was a lot easier, less complex and more than goof enough. Kotlin, for example, mirrors this
 *     decision, though it flows a bit nicer with extension methods.
 *   </li>
 * </ul>
 */
package edu.kit.informatik.dawn;
// @formatter:on