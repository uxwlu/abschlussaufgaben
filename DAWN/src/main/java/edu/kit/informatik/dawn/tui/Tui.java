package edu.kit.informatik.dawn.tui;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.commandsystem.command.CommandExecutionResult;
import edu.kit.informatik.commandsystem.command.CommandResult;
import edu.kit.informatik.commandsystem.exceptions.CommandMiscException;
import edu.kit.informatik.commandsystem.exceptions.CommandSyntaxException;
import edu.kit.informatik.commandsystem.tree.CommandDispatcher;
import edu.kit.informatik.commandsystem.tree.CommandNode;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.util.Consumer;
import java.util.List;

/**
 * A terminal user interface.
 *
 * @author uxwlu
 * @version 1.0
 */
public class Tui implements Consumer<List<CommandNode<Game>>> {

  private static final char ARGUMENT_SEPARATOR = ':';
  private static final String COMMAND_ARGUMENT_SEPARATOR = " ";
  private final CommandDispatcher<Game> commandDispatcher;
  private final Game game;

  /**
   * Creates a new tui for the given game.
   *
   * @param game the game
   */
  public Tui(Game game) {
    this.commandDispatcher = new CommandDispatcher<>(
        ARGUMENT_SEPARATOR,
        COMMAND_ARGUMENT_SEPARATOR
    );
    this.game = game;
    game.getCommands().setChangeListener(this);

    accept(game.getCommands().getCommands());
  }

  /**
   * Runs the tui.
   */
  public void run() {
    while (true) {
      try {
        String input = Terminal.readLine();
        CommandExecutionResult result = commandDispatcher.execute(game, input);

        if (result.getResult() == CommandResult.EXIT) {
          return;
        }
      } catch (CommandSyntaxException | CommandMiscException e) {
        Terminal.printError(decaptialise(e.getMessage()));
      }
    }
  }

  @Override
  public void accept(List<CommandNode<Game>> commandNodes) {
    commandDispatcher.clearCommands();

    for (CommandNode<Game> node : commandNodes) {
      commandDispatcher.addCommand(node);
    }
  }

  /**
   * Decapitalises a word by making the first character lowercase.
   *
   * @param input the input string
   * @return the string with the first char in lower case
   */
  private static String decaptialise(String input) {
    return input.isEmpty() ? "" : input.substring(0, 1).toLowerCase() + input.substring(1);
  }
}
