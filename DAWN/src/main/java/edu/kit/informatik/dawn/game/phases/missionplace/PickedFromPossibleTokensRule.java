package edu.kit.informatik.dawn.game.phases.missionplace;

import edu.kit.informatik.commandsystem.util.Optional;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.moves.MissionControlPlacementMove;
import edu.kit.informatik.dawn.rules.MoveRule;
import edu.kit.informatik.dawn.token.Token;
import edu.kit.informatik.dawn.util.StreamSupplement;
import java.util.List;

/**
 * Enforces that the user picks a token from a given list.
 *
 * @author uxwlu
 * @version 1.0
 */
class PickedFromPossibleTokensRule implements MoveRule<MissionControlPlacementMove> {

  private final List<? extends Token> possibleTokens;

  /**
   * Creates a new rule.
   *
   * @param possibleTokens the possible tokens the player can choose from
   */
  PickedFromPossibleTokensRule(List<? extends Token> possibleTokens) {
    this.possibleTokens = possibleTokens;
  }

  @Override
  public Class<MissionControlPlacementMove> getMoveClass() {
    return MissionControlPlacementMove.class;
  }

  @Override
  public Optional<String> validate(MissionControlPlacementMove move, Game game) {
    if (!possibleTokens.contains(move.getToken())) {
      List<String> tokenLengths = StreamSupplement
          .map(possibleTokens, token -> Integer.toString(token.getLength()));
      String possibleTokens = String.join(", ", tokenLengths);
      return Optional.of("you must use one of the following tokens: " + possibleTokens + "!");
    }
    return Optional.empty();
  }
}
