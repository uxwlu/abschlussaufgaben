package edu.kit.informatik.dawn.game.phases.naturemove;

import edu.kit.informatik.commandsystem.util.Optional;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.moves.NatureMove;
import edu.kit.informatik.dawn.rules.MoveRule;

/**
 * Enforces that the path is not too long.
 *
 * @author uxwlu
 * @version 1.0
 */
class PathIsNotTooLongRule implements MoveRule<NatureMove> {

  private final int maxLength;

  /**
   * Creates a new rule.
   *
   * @param maxLength the maximum length of the path
   */
  PathIsNotTooLongRule(int maxLength) {
    this.maxLength = maxLength;
  }

  @Override
  public Class<NatureMove> getMoveClass() {
    return NatureMove.class;
  }

  @Override
  public Optional<String> validate(NatureMove move, Game game) {
    int pathLength = move.getPath().size();

    // +1 as the start position does not count
    if (pathLength > maxLength + 1) {
      return Optional.of("you can not move that far. Maybe there is a more efficient route?");
    }

    return Optional.empty();
  }
}
