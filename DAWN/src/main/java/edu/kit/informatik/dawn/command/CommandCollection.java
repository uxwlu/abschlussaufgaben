package edu.kit.informatik.dawn.command;

import edu.kit.informatik.commandsystem.tree.CommandNode;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.game.Phase;
import edu.kit.informatik.dawn.game.PhaseType;
import edu.kit.informatik.dawn.util.Consumer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * An observable collection of commands which contains global and phase bound commands.
 *
 * @author uxwlu
 * @version 1.0
 */
public class CommandCollection {

  private final List<CommandNode<Game>> gameCommands;
  private final Map<PhaseType, List<CommandNode<Game>>> phaseCommands;
  private Consumer<List<CommandNode<Game>>> changeListener;
  private PhaseType currentPhase;

  /**
   * Creates a new command collection.
   */
  public CommandCollection() {
    this.changeListener = commandNodes -> {
    };
    this.gameCommands = new ArrayList<>();
    this.phaseCommands = new HashMap<>();
  }

  /**
   * Sets the command change listener.
   *
   * @param changeListener the command change listener
   */
  public void setChangeListener(Consumer<List<CommandNode<Game>>> changeListener) {
    this.changeListener = changeListener;
  }

  /**
   * Adds a game command node. These commands are active in all phases.
   *
   * @param node the command node
   */
  public void addGameCommand(CommandNode<Game> node) {
    gameCommands.add(node);

    notifyChange();
  }

  /**
   * Adds a new command that is bound to a phase.
   *
   * @param phase the phase the command is bound to
   * @param command the command
   */
  public void addPhaseboundCommand(PhaseType phase, CommandNode<Game> command) {
    List<CommandNode<Game>> commands = phaseCommands.getOrDefault(phase, new ArrayList<>());
    commands.add(command);
    phaseCommands.put(phase, commands);
  }

  /**
   * Changes the phase bound commands.
   *
   * @param newPhase the new phase
   */
  public void phaseSwitched(Phase newPhase) {
    this.currentPhase = newPhase.getType();

    notifyChange();
  }

  private void notifyChange() {
    changeListener.accept(getCommands());
  }

  /**
   * Returns all commands.
   *
   * @return all commands
   */
  public List<CommandNode<Game>> getCommands() {
    ArrayList<CommandNode<Game>> commands = new ArrayList<>();

    commands.addAll(gameCommands);
    commands.addAll(getPhaseCommands());

    return commands;
  }

  /**
   * Adds a temporary command node.
   *
   * @return all commands for that phase
   */
  private List<CommandNode<Game>> getPhaseCommands() {
    return Collections.unmodifiableList(
        phaseCommands.getOrDefault(currentPhase, Collections.emptyList())
    );
  }
}
