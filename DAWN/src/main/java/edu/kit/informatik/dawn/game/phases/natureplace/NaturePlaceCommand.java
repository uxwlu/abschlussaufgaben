package edu.kit.informatik.dawn.game.phases.natureplace;

import static edu.kit.informatik.dawn.command.types.PositiveFieldPositionType.positivePosition;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.commandsystem.tree.CommandNode;
import edu.kit.informatik.commandsystem.util.Optional;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.game.phases.natureroll.NatureDecidingMovePhase;
import edu.kit.informatik.dawn.moves.NaturePlacementMove;
import edu.kit.informatik.dawn.token.NatureToken;
import edu.kit.informatik.dawn.util.Point;

/**
 * A command that allows nature to place down a token.
 *
 * @author uxwlu
 * @version 1.0
 */
public class NaturePlaceCommand {

  /**
   * Creates a command that allows you to place the given nature token.
   *
   * @return the command
   */
  public static CommandNode<Game> getCommand() {
    return CommandNode.<Game>builder("set-vc")
        .withArgument("position", positivePosition())
        .withCommand(commandContext -> {
          Game game = commandContext.getSource();
          Point position = commandContext.getArgument(Point.class, "position");

          NatureToken token = game.getNature().getCurrentToken();

          Optional<String> error = game.applyMove(NaturePlacementMove.create(token, position));

          if (error.isPresent()) {
            Terminal.printError(error.get());
            return;
          }
          Terminal.printLine("OK");
          game.switchPhase(new NatureDecidingMovePhase(game.getDie()));
        })
        .build();
  }
}
