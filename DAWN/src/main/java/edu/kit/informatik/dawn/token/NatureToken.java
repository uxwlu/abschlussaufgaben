package edu.kit.informatik.dawn.token;

import edu.kit.informatik.commandsystem.util.Optional;
import edu.kit.informatik.dawn.util.Point;

/**
 * A token for the nature player.
 *
 * @author uxwlu
 * @version 1.0
 */
public class NatureToken extends Token {

  private static final int TOKEN_SIZE = 1;

  private Point position;
  private final NatureTokenType type;

  /**
   * Creates a new token.
   *
   * @param type the type of this token
   */
  public NatureToken(NatureTokenType type) {
    super(TOKEN_SIZE);
    this.type = type;
  }

  /**
   * Returns the position of the token.
   *
   * @return the position of the token
   */
  public Optional<Point> getPosition() {
    return Optional.ofNullable(position);
  }

  /**
   * Sets the position of this token.
   *
   * @param position the position of the token
   */
  public void setPosition(Point position) {
    this.position = position;
  }

  @Override
  public String abbreviate() {
    return type.getAbbreviation();
  }

  @Override
  public String toString() {
    return "NatureToken{"
        + "position=" + position
        + ", type=" + type
        + '}';
  }

  /**
   * The type of the token.
   */
  public enum NatureTokenType {
    /**
     * Ceres
     */
    CERES("C"),
    /**
     * Vesta
     */
    VESTA("V");

    private final String abbreviation;

    /**
     * Creates a new nature token type.
     *
     * @param abbreviation the abbreviation for the type
     */
    NatureTokenType(String abbreviation) {
      this.abbreviation = abbreviation;
    }

    /**
     * Returns the token's abbreviation.
     *
     * @return the abbreviation
     */
    private String getAbbreviation() {
      return abbreviation;
    }
  }
}
