package edu.kit.informatik.dawn.game.phases.naturemove;

import edu.kit.informatik.commandsystem.util.Optional;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.moves.NatureMove;
import edu.kit.informatik.dawn.rules.MoveRule;
import edu.kit.informatik.dawn.util.Point;
import java.util.List;

/**
 * Enforces that the path only consists of elementary steps.
 *
 * @author uxwlu
 * @version 1.0
 */
class PathIsElementaryRule implements MoveRule<NatureMove> {

  @Override
  public Class<NatureMove> getMoveClass() {
    return NatureMove.class;
  }

  @Override
  public Optional<String> validate(NatureMove move, Game game) {
    if (move.isEmpty()) {
      return Optional.empty();
    }

    return consistsOfElementarySteps(move.getPath());
  }

  private Optional<String> consistsOfElementarySteps(List<Point> path) {
    Point current = path.get(0);

    for (Point point : path.subList(1, path.size())) {
      if (current.manhattenTo(point) != 1) {
        return Optional.of(String.format(
            "The step from %s to %s has a distance of %d. Allowed is 1 (ONE)!",
            current.toIoCoordinateSystem(), point.toIoCoordinateSystem(), current.manhattenTo(point)
        ));
      }
      current = point;
    }

    return Optional.empty();
  }
}
