package edu.kit.informatik.dawn.game.phases.natureroll;

import edu.kit.informatik.dawn.game.PhaseType;
import edu.kit.informatik.dawn.game.phases.AbstractPhase;
import edu.kit.informatik.dawn.rules.MoveRule;
import edu.kit.informatik.dawn.util.Die;
import java.util.Collections;
import java.util.List;

/**
 * The phase where nature can roll to decide the move.
 *
 * @author uxwlu
 * @version 1.0
 */
public class NatureDecidingMovePhase extends AbstractPhase {

  private final Die<Integer> die;

  /**
   * Creates a new phase.
   *
   * @param die the die to use
   */
  public NatureDecidingMovePhase(Die<Integer> die) {
    this.die = die;
  }

  @Override
  public PhaseType getType() {
    return PhaseType.DECIDING_MISSION_CONTROL_TOKEN;
  }

  @Override
  protected List<MoveRule<?>> rules() {
    return Collections.singletonList(
        new RolledSymbolOnDieRule(die)
    );
  }
}
