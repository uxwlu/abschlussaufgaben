package edu.kit.informatik.dawn.game.phases;

import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.game.Phase;
import edu.kit.informatik.dawn.rules.MoveRule;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A skeleton implementation of a {@link Phase}.
 *
 * @author uxwlu
 * @version 1.0
 */
public abstract class AbstractPhase implements Phase {

  private List<MoveRule<?>> rules;

  /**
   * Creates a new abstract phase.
   */
  protected AbstractPhase() {
  }

  /**
   * Returns all rules this phase needs. If the rule list is empty, no move will be possible.
   *
   * @return all rules this phase needs
   */
  protected abstract List<MoveRule<?>> rules();

  @Override
  public void setup(Game game) {

  }

  @Override
  public List<MoveRule<?>> getRules() {
    if (rules == null) {
      rules = new ArrayList<>(rules());
    }
    return Collections.unmodifiableList(rules);
  }
}
