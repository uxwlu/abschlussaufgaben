package edu.kit.informatik.dawn.util;

import edu.kit.informatik.commandsystem.util.Function;
import edu.kit.informatik.commandsystem.util.Optional;
import edu.kit.informatik.commandsystem.util.Supplier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * Contains helper functions for things that a stream would solve nicely: Transformation, filtering,
 * grouping, and matching.
 *
 * @author uxwlu
 * @version 1.0
 */
public final class StreamSupplement {

  private StreamSupplement() {
    // utility class
    throw new UnsupportedOperationException();
  }

  /**
   * Eagerly maps a collection.
   *
   * @param input the input collection
   * @param mapping the mapping function
   * @param <T> the original type
   * @param <R> the mapped type
   * @return the mapped collection
   */
  public static <T, R> List<R> map(Iterable<T> input, Function<T, R> mapping) {
    List<R> result = new ArrayList<>();

    for (T t : input) {
      result.add(mapping.apply(t));
    }

    return result;
  }

  /**
   * Returns a new collection consisting of all elements that matched the filter.
   *
   * @param input the input collection
   * @param filter the filter
   * @param <T> the original type
   * @return the filtered collection
   */
  public static <T> List<T> filter(Iterable<T> input, Predicate<T> filter) {
    List<T> result = new ArrayList<>();

    for (T t : input) {
      if (filter.test(t)) {
        result.add(t);
      }
    }

    return result;
  }

  /**
   * Groups a collection by some property.
   *
   * @param input the input
   * @param keyExtractor the key extraction function
   * @param mapFactory the map factory to use
   * @param <K> the type of the key
   * @param <V> the type of the value
   * @param <M> the map type
   * @return the grouped map
   */
  public static <K, V, M extends Map<K, List<V>>> M groupingBy(Iterable<V> input,
      Function<V, K> keyExtractor, Supplier<M> mapFactory) {
    M map = mapFactory.get();

    for (V value : input) {
      K key = keyExtractor.apply(value);

      List<V> list = map.getOrDefault(key, new ArrayList<>());
      list.add(value);
      map.put(key, list);
    }

    return map;
  }

  /**
   * Checks if all elements in the supplied collection match the predicate.
   *
   * @param collection the collection
   * @param predicate the predicate
   * @param <T> the type of the elements
   * @return true if all match
   */
  public static <T> boolean allMatch(Iterable<T> collection, Predicate<T> predicate) {
    for (T t : collection) {
      if (!predicate.test(t)) {
        return false;
      }
    }
    return true;
  }

  /**
   * Checks if at least one element in the supplied collection matches the predicate.
   *
   * @param collection the collection
   * @param predicate the predicate
   * @param <T> the type of the elements
   * @return true if any element matches
   */
  public static <T> boolean anyMatch(Iterable<T> collection, Predicate<T> predicate) {
    return first(collection, predicate).isPresent();
  }

  /**
   * Returns the first element matching the predicate.
   *
   * @param collection the collection
   * @param predicate the predicate
   * @param <T> the type of the elements
   * @return the first element matching the predicate
   */
  public static <T> Optional<T> first(Iterable<T> collection, Predicate<T> predicate) {
    for (T t : collection) {
      if (predicate.test(t)) {
        return Optional.of(t);
      }
    }
    return Optional.empty();
  }

  /**
   * Sorts the collection and puts the result in a new list.
   *
   * @param collection the collection
   * @param comparator the comparator to use
   * @param <T> the type of the elements
   * @return the sorted collection
   */
  public static <T> List<T> sortedBy(Collection<T> collection, Comparator<T> comparator) {
    List<T> list = new ArrayList<>(collection);

    list.sort(comparator);

    return list;
  }
}
