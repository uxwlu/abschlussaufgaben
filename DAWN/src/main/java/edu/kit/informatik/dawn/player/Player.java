package edu.kit.informatik.dawn.player;

import edu.kit.informatik.dawn.token.Token;
import edu.kit.informatik.dawn.token.TokenSet;

/**
 * Represents a DAWN player.
 *
 * @param <T> the token type for the player
 * @author uxwlu
 * @version 1.0
 */
public interface Player<T extends Token> {

  /**
   * Returns the token set for this player.
   *
   * @return the token set for this player
   */
  TokenSet<T> getTokens();

  /**
   * Resets this player to their initial state.
   */
  void reset();
}
