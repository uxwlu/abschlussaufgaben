package edu.kit.informatik.dawn.game.phases.missionplace;

import edu.kit.informatik.commandsystem.util.Optional;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.moves.MissionControlPlacementMove;
import edu.kit.informatik.dawn.rules.MoveRule;

/**
 * Enforce that the token is not placed diagonally.
 *
 * @author uxwlu
 * @version 1.0
 */
class TokenIsNotDiagonalRule implements MoveRule<MissionControlPlacementMove> {

  @Override
  public Class<MissionControlPlacementMove> getMoveClass() {
    return MissionControlPlacementMove.class;
  }

  @Override
  public Optional<String> validate(MissionControlPlacementMove move, Game game) {
    if (move.getPlacementArea().getWidth() > 1 && move.getPlacementArea().getHeight() > 1) {
      return Optional.of("you can not place it diagonally!");
    }
    return Optional.empty();
  }
}
