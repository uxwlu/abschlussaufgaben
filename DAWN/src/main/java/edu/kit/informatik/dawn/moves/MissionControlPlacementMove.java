package edu.kit.informatik.dawn.moves;

import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.game.phases.naturemove.NatureMovesPhase;
import edu.kit.informatik.dawn.token.MissionControlToken;
import edu.kit.informatik.dawn.util.Rectangle;

/**
 * Mission control placing down a token.
 *
 * @author uxwlu
 * @version 1.0
 */
public interface MissionControlPlacementMove extends Move {

  /**
   * Returns the token the user wanted to place.
   *
   * @return the token the user wanted to place
   */
  MissionControlToken getToken();

  /**
   * Returns the area the user wanted to place it on.
   *
   * @return the area the user wanted to place it on
   */
  Rectangle getPlacementArea();

  /**
   * Creates a new placement move.
   *
   * @param token the token to place down
   * @param area the area to place it down on
   * @return the move
   */
  static MissionControlPlacementMove of(MissionControlToken token, Rectangle area) {
    return new MissionControlPlacementMove() {
      @Override
      public MissionControlToken getToken() {
        return token;
      }

      @Override
      public Rectangle getPlacementArea() {
        return area;
      }

      @Override
      public void apply(Game game) {
        game.getGameBoard().set(area, token);

        game.getMissionControl().getTokens().tokenPlaced(token);

        game.switchPhase(new NatureMovesPhase(
            Math.max(getPlacementArea().getHeight(), getPlacementArea().getWidth())
        ));
      }
    };
  }
}
