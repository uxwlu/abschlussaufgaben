package edu.kit.informatik.dawn.game.phases.naturemove;

import edu.kit.informatik.commandsystem.util.Optional;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.game.PhaseType;
import edu.kit.informatik.dawn.game.phases.AbstractPhase;
import edu.kit.informatik.dawn.moves.NatureMove;
import edu.kit.informatik.dawn.rules.MoveRule;
import edu.kit.informatik.dawn.token.NatureToken;
import edu.kit.informatik.dawn.util.Point;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * A phase that allows nature to move.
 *
 * @author uxwlu
 * @version 1.0
 */
public class NatureMovesPhase extends AbstractPhase {

  private final int maxDistance;

  /**
   * Creates a new phase.
   *
   * @param maxDistance the maximum distance nature can move
   */
  public NatureMovesPhase(int maxDistance) {
    this.maxDistance = maxDistance;
  }

  @Override
  public PhaseType getType() {
    return PhaseType.NATURE_MOVES;
  }

  @Override
  public void setup(Game game) {
    NatureToken natureToken = game.getNature().getCurrentToken();

    Optional<Point> position = natureToken.getPosition();
    if (!position.isPresent()) {
      return;
    }

    // You can not move so I will move for you :)
    if (!game.getGameBoard().hasFreeNeighbour(position.get())) {
      game.applyMove(NatureMove.of(natureToken, Collections.singletonList(position.get())));
    }
  }

  @Override
  protected List<MoveRule<?>> rules() {
    return Arrays.asList(
        new PathIsFreeRule(),
        new PathIsNotEmptyRule(),
        new PathIsElementaryRule(),
        new PathIsNotTooLongRule(maxDistance),
        new MoveMustStartAtTokenRule()
    );
  }
}
