package edu.kit.informatik.dawn.moves;

import edu.kit.informatik.dawn.game.Game;

/**
 * A move that can be applied to a game.
 *
 * @author uxwlu
 * @version 1.0
 */
public interface Move {

  /**
   * Applies the move to the game.
   *
   * @param game the game to apply it to
   */
  void apply(Game game);
}
