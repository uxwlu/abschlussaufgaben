package edu.kit.informatik.dawn.moves;

import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.token.NatureToken;
import edu.kit.informatik.dawn.util.Point;

/**
 * A move that places a nature token.
 *
 * @author uxwlu
 * @version 1.0
 */
public interface NaturePlacementMove extends Move {

  /**
   * Returns the token to place.
   *
   * @return the token to place
   */
  NatureToken getToken();

  /**
   * Returns the point to place it at.
   *
   * @return the point to place it at
   */
  Point getPoint();

  /**
   * Creates a new {@link NaturePlacementMove}.
   *
   * @param token the token
   * @param point the point
   * @return the created nature placement move
   */
  static NaturePlacementMove create(NatureToken token, Point point) {
    return new NaturePlacementMove() {
      @Override
      public NatureToken getToken() {
        return token;
      }

      @Override
      public Point getPoint() {
        return point;
      }

      @Override
      public void apply(Game game) {
        game.getGameBoard().set(point, token);

        getToken().setPosition(getPoint());
      }
    };
  }
}
