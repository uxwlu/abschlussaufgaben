package edu.kit.informatik.dawn.token;

/**
 * Represents a single token.
 *
 * @author uxwlu
 * @version 1.0
 */
public abstract class Token implements Comparable<Token> {

  private final int length;

  /**
   * Creates a new token.
   *
   * @param length the length of the token
   */
  public Token(int length) {
    this.length = length;
  }

  /**
   * Returns the length of the token.
   *
   * @return the length of the token
   */
  public int getLength() {
    return length;
  }

  /**
   * Abbreviates this token.
   *
   * @return an abbreviation for this token
   */
  public abstract String abbreviate();

  /**
   * Compares the tokens based on their length.
   *
   * @param o the other token
   * @return -1 if this token is shorter, 0 if they are of equal length and 1 otherwise
   */
  @Override
  public int compareTo(Token o) {
    return Integer.compare(getLength(), o.getLength());
  }

  @Override
  public String toString() {
    return abbreviate() + "-" + getLength();
  }
}
