package edu.kit.informatik.dawn.game.phases.natureplace;

import edu.kit.informatik.dawn.game.PhaseType;
import edu.kit.informatik.dawn.game.phases.AbstractPhase;
import edu.kit.informatik.dawn.rules.MoveRule;
import edu.kit.informatik.dawn.token.NatureToken;
import java.util.Arrays;
import java.util.List;

/**
 * One of two phases where you hunt a natureToken.
 *
 * @author uxwlu
 * @version 1.0
 */
public class NaturePlacePhase extends AbstractPhase {

  private final NatureToken natureToken;

  /**
   * Creates a new phase natureplace for a nature token.
   *
   * @param natureToken the natureToken
   */
  public NaturePlacePhase(NatureToken natureToken) {
    this.natureToken = natureToken;
  }

  @Override
  public PhaseType getType() {
    return PhaseType.PLACING_NATURE;
  }

  @Override
  protected List<MoveRule<?>> rules() {
    return Arrays.asList(
        new CantPlaceAgainRule(),
        new MovesCorrectTokenRule(natureToken),
        new PositionIsOnBoardRule(),
        new PositionIsFreeRule()
    );
  }
}
