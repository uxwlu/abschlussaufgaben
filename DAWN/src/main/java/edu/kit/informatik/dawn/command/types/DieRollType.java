package edu.kit.informatik.dawn.command.types;

import edu.kit.informatik.commandsystem.arguments.StatelessArgumentType;
import edu.kit.informatik.commandsystem.exceptions.CommandSyntaxException;
import edu.kit.informatik.commandsystem.util.Optional;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.util.Die;

/**
 * Contains {@link edu.kit.informatik.commandsystem.arguments.ArgumentType}s for rolling a die.
 *
 * @author uxwlu
 * @version 1.0
 */
// This class is separate from the rule, as the command argument type is fundamentally different and
// not related. The related concept is *the die*.
public class DieRollType {

  /**
   * Returns an {@link edu.kit.informatik.commandsystem.arguments.ArgumentType} for a die roll.
   *
   * @param die the die
   * @param <T> the type of the die sides
   * @return the argument type
   */
  public static <T> StatelessArgumentType<T, Game> dieRoll(Die<T> die) {
    return stringReader -> {
      String argument = stringReader.readArgument();

      Optional<T> dieResult = die.roll(argument);

      if (!dieResult.isPresent()) {
        throw new CommandSyntaxException(String.format("Invalid die symbol: '%s'", argument));
      }

      return dieResult.get();
    };
  }
}
