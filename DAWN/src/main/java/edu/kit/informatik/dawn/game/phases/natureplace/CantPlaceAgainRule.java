package edu.kit.informatik.dawn.game.phases.natureplace;

import edu.kit.informatik.commandsystem.util.Optional;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.moves.NaturePlacementMove;
import edu.kit.informatik.dawn.rules.MoveRule;

/**
 * Enforce that the user can not place a token twice.
 *
 * @author uxwlu
 * @version 1.0
 */
class CantPlaceAgainRule implements MoveRule<NaturePlacementMove> {

  @Override
  public Class<NaturePlacementMove> getMoveClass() {
    return NaturePlacementMove.class;
  }

  @Override
  public Optional<String> validate(NaturePlacementMove move, Game game) {
    if (move.getToken().getPosition().isPresent()) {
      return Optional.of("this token was already placed!");
    }
    return Optional.empty();
  }
}
