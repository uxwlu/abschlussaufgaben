package edu.kit.informatik.dawn.command.commands;

import edu.kit.informatik.commandsystem.util.Function;
import edu.kit.informatik.dawn.token.MissionControlToken;
import edu.kit.informatik.dawn.token.Token;

class FancyRenderer implements Function<Token, String> {

  private static final String RED = "\u001B[31m";
  private static final String RESET = "\u001B[0m";
  private static final String PURPLE = "\u001B[35m";
  private static final String BLACKISH = "\u001B[90m";

  /**
   * Renders a single token.
   *
   * Accepts {@code null} to indicate the absence of a token.
   *
   * @param token the token to render
   * @return the rendered representation
   */
  @Override
  public String apply(Token token) {
    return rendered(token) + " ";
  }

  private String rendered(Token token) {
    if (token == null) {
      return BLACKISH + "-" + RESET;
    }
    if (token instanceof MissionControlToken) {
      return RED + token.abbreviate() + RESET;
    } else {
      return PURPLE + token.abbreviate() + RESET;
    }
  }

}
