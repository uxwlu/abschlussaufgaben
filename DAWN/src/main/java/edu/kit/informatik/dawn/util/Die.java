package edu.kit.informatik.dawn.util;

import edu.kit.informatik.commandsystem.util.Optional;

/**
 * A die you can roll.
 *
 * @param <T> the type of the die
 * @author uxwlu
 * @version 1.0
 */
public interface Die<T> {

  /**
   * Verifies the die roll is valid and returns the result.
   *
   * @param value the rolled value
   * @return the result or an empty optional if the roll was invalid
   */
  Optional<T> roll(String value);

  /**
   * Returns whether the value is contained in the die.
   *
   * @param value the value
   * @return true if the die contains the value
   */
  boolean containsValue(T value);
}
