package edu.kit.informatik.dawn.game.phases.end;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.commandsystem.tree.CommandNode;
import edu.kit.informatik.commandsystem.util.Optional;
import edu.kit.informatik.dawn.board.Board;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.token.NatureToken;
import edu.kit.informatik.dawn.util.Point;
import edu.kit.informatik.dawn.util.StreamSupplement;
import java.util.ArrayDeque;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;

/**
 * A command to show thw score.
 *
 * @author uxwlu
 * @version 1.0
 */
// Showing the result in this way is solely a UI feature. Moving over to a dedicated result
// function later on would be an easy transition, if it would ever be needed.
public class ShowResultCommand {

  /**
   * A command to show thw score.
   *
   * @return the command
   */
  public static CommandNode<Game> getCommand() {
    return CommandNode.<Game>builder("show-result")
        .withCommand(commandContext -> {
          Game game = commandContext.getSource();

          List<Optional<Point>> positionOptionals = StreamSupplement
              .map(game.getNature().getTokens().getAll(), NatureToken::getPosition);

          List<Point> positions = StreamSupplement.map(positionOptionals, Optional::get);

          List<Integer> distances = StreamSupplement
              .map(positions, point -> getFreeConnected(point, game.getGameBoard()));

          int max = Collections.max(distances);
          int min = Collections.min(distances);

          int score = max + Math.abs(max - min);

          Terminal.printLine(score);
        })
        .build();
  }

  private static int getFreeConnected(Point start, Board board) {
    Queue<Point> open = new ArrayDeque<>();
    Set<Point> closed = new HashSet<>();

    open.add(start);

    while (!open.isEmpty()) {
      Point point = open.poll();
      if (closed.contains(point)) {
        continue;
      }
      closed.add(point);

      open.addAll(expand(point, board));
    }

    // do not count yourself
    return closed.size() - 1;
  }

  private static List<Point> expand(Point point, Board board) {
    return StreamSupplement.filter(point.getAdjacent(), board::isFree);
  }
}
