package edu.kit.informatik.dawn.game.phases.natureplace;

import edu.kit.informatik.commandsystem.util.Optional;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.moves.NaturePlacementMove;
import edu.kit.informatik.dawn.rules.MoveRule;

/**
 * Enforces that the position is free.
 *
 * @author uxwlu
 * @version 1.0
 */
class PositionIsFreeRule implements MoveRule<NaturePlacementMove> {

  @Override
  public Class<NaturePlacementMove> getMoveClass() {
    return NaturePlacementMove.class;
  }

  @Override
  public Optional<String> validate(NaturePlacementMove move, Game game) {
    if (!game.getGameBoard().isFree(move.getPoint())) {
      return Optional.of("the cell is not free!");
    }
    return Optional.empty();
  }
}
