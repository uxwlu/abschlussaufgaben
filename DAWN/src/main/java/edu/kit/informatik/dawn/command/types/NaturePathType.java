package edu.kit.informatik.dawn.command.types;

import edu.kit.informatik.commandsystem.arguments.ArgumentType;
import edu.kit.informatik.commandsystem.arguments.RepeatedArgumentType;
import edu.kit.informatik.commandsystem.util.CommandInvariant;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.util.Point;
import java.util.List;

/**
 * An {@link ArgumentType} parsing a path for a nature token.
 *
 * @author uxwlu
 * @version 1.0
 */
public class NaturePathType {

  /**
   * Reads a path for a nature nature token.
   *
   * @param length the length of the path
   * @return the argument type
   */
  public static ArgumentType<List<Point>, Game> naturePath(int length) {
    return RepeatedArgumentType.repeated(
        1,
        length,
        PositiveFieldPositionType.positivePosition(),
        ':',
        getInvariant()
    );
  }

  private static CommandInvariant<Game, List<Point>> getInvariant() {
    return CommandInvariant.betweenElements(
        (point, point2) -> point.manhattenTo(point2) == 1,
        "The distance between points must be 1!"
    );
  }
}
