package edu.kit.informatik.dawn.rules;

import edu.kit.informatik.commandsystem.util.Optional;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.moves.Move;
import java.util.ArrayList;
import java.util.List;

/**
 * A collection of rules.
 *
 * @author uxwlu
 * @version 1.0
 */
public class Ruleset {

  private final List<MoveRule<?>> rules;

  /**
   * Creates a new rule set.
   */
  public Ruleset() {
    this.rules = new ArrayList<>();
  }

  /**
   * Adds a new rule.
   *
   * @param rule the rule
   * @param <T> the type of the rule
   */
  public <T extends Move> void addRule(MoveRule<T> rule) {
    rules.add(rule);
  }

  /**
   * Removes a rule.
   *
   * @param rule the rule to remove
   * @param <T> the type of the rule
   */
  public <T extends Move> void removeRule(MoveRule<T> rule) {
    rules.remove(rule);
  }

  /**
   * Checks whether a move conforms to all rules.
   *
   * <p>If no rules are registered, the result will be an <em>error message</em>.</p>
   *
   * @param move the move
   * @param game the game
   * @param <T> the type of the move
   * @return true if the move conforms to all rules
   */
  public <T extends Move> Optional<String> check(T move, Game game) {
    boolean checked = false;

    for (MoveRule<?> rule : rules) {
      if (rule.getMoveClass().isAssignableFrom(move.getClass())) {
        checked = true;

        // This is actually safe, but beyond javac's capability. It is safe, as we explicitly
        // checked that in the if above.
        @SuppressWarnings("unchecked")
        MoveRule<T> castRule = (MoveRule<T>) rule;

        Optional<String> result = castRule.validate(castRule.getMoveClass().cast(move), game);

        if (result.isPresent()) {
          return result;
        }
      }
    }

    // If no rule was registered for a move, we will assume it is invalid. This protects against
    // doing anything when the phase registered no rules.
    if (!checked) {
      return Optional
          .of(
              "I do not have any rule for you,"
                  + " so I will have to fail you to protect my integrity :("
          );
    }

    return Optional.empty();
  }
}
