package edu.kit.informatik.dawn.player;

import edu.kit.informatik.dawn.token.MissionControlToken;
import edu.kit.informatik.dawn.token.TokenSet;
import java.util.List;

/**
 * The mission control player.
 *
 * @author uxwlu
 * @version 1.0
 */
public class MissionControl implements Player<MissionControlToken> {

  private final TokenSet<MissionControlToken> tokens;

  /**
   * Creates a new player.
   *
   * @param tokens the tokens
   */
  public MissionControl(List<MissionControlToken> tokens) {
    this.tokens = new TokenSet<>(tokens);
  }

  @Override
  public TokenSet<MissionControlToken> getTokens() {
    return tokens;
  }

  @Override
  public void reset() {
    tokens.refill();
  }
}
