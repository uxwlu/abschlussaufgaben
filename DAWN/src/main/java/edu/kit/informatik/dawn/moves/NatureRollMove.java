package edu.kit.informatik.dawn.moves;

import static edu.kit.informatik.dawn.util.StreamSupplement.sortedBy;

import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.game.phases.missionplace.MissionControlPlacesTokenPhase;
import edu.kit.informatik.dawn.player.MissionControl;
import edu.kit.informatik.dawn.token.MissionControlToken;
import edu.kit.informatik.dawn.token.Token;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Nature rolled.
 *
 * @author uxwlu
 * @version 1.0
 */
public interface NatureRollMove extends Move {

  /**
   * Returns the rolled value.
   *
   * @return the rolled value
   */
  int getRolledValue();

  /**
   * Returns the mission control player.
   *
   * @return the mission control player
   */
  MissionControl getMissionControl();

  /**
   * Returns all tokens mission control is allowed to place.
   *
   * @return all tokens mission control is allowed to place.
   */
  default List<MissionControlToken> getPossibleTokens() {
    List<MissionControlToken> remainingTokens = sortedBy(
        getMissionControl().getTokens().getRemaining(), Token::compareTo
    );

    if (remainingTokens.size() <= 1) {
      return remainingTokens;
    }

    MissionControlToken last = remainingTokens.get(0);

    // only one left or there is no lower token
    if (last.getLength() >= getRolledValue()) {
      return Collections.singletonList(last);
    }

    for (MissionControlToken token : remainingTokens.subList(1, remainingTokens.size())) {
      // exact match
      if (token.getLength() == getRolledValue()) {
        return Collections.singletonList(token);
      }
      // we have found a bigger one, so return the last one and this
      else if (token.getLength() > getRolledValue()) {
        return Arrays.asList(last, token);
      }

      last = token;
    }

    return remainingTokens;
  }


  /**
   * Creates a new roll move.
   *
   * @param value the rolled value
   * @param missionControl the mission control player
   * @return the created move
   */
  static NatureRollMove of(int value, MissionControl missionControl) {
    return new NatureRollMove() {
      @Override
      public int getRolledValue() {
        return value;
      }

      @Override
      public MissionControl getMissionControl() {
        return missionControl;
      }

      @Override
      public void apply(Game game) {
        game.switchPhase(new MissionControlPlacesTokenPhase(getPossibleTokens()));
      }
    };
  }
}
