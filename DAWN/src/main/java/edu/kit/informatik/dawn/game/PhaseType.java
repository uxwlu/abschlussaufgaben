package edu.kit.informatik.dawn.game;

/**
 * The different game phases.
 *
 * @author uxwlu
 * @version 1.0
 */
public enum PhaseType {
  /**
   * Initial phase where nature places down the nature tokens.
   */
  PLACING_NATURE,
  /**
   * Nature decides which token mission control needs to place.
   */
  DECIDING_MISSION_CONTROL_TOKEN,
  /**
   * Mission control places their token.
   */
  PLACING_MISSION_CONTROL,
  /**
   * Nature moves.
   */
  NATURE_MOVES,
  /**
   * The game has ended.
   */
  GAME_ENDED
}
