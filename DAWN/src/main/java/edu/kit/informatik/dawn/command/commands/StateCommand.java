package edu.kit.informatik.dawn.command.commands;

import static edu.kit.informatik.dawn.command.types.PositiveFieldPositionType.positivePosition;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.commandsystem.tree.CommandNode;
import edu.kit.informatik.commandsystem.util.Function;
import edu.kit.informatik.dawn.board.Board;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.token.Token;
import edu.kit.informatik.dawn.util.Point;

/**
 * A command to print the state of a single field.
 *
 * @author uxwlu
 * @version 1.0
 */
public class StateCommand {

  // I made a small ANSI renderer to be able to actually read the output, so this is quite generic
  // The print and state do not share a single renderer, as the ANSI renderer is not needed for
  // printing a single char.
  // The ANSI renderer is not included in the submission, I just wanted to explain the reasoning
  // for this weirdly generic and (at first glance) duplicated statement
  private static final Function<Token, String> RENDERER = new DefaultTokenRenderer();

  /**
   * A command to print the state of a single field.
   *
   * @return the command
   */
  public static CommandNode<Game> getCommand() {
    return CommandNode.<Game>builder("state")
        .withArgument("position", positivePosition())
        .withCommand(commandContext -> {
          Point position = commandContext.getArgument(Point.class, "position");

          Board board = commandContext.getSource().getGameBoard();

          String displayString = RENDERER.apply(board.get(position).orElse(null));

          Terminal.printLine(displayString);
        })
        .build();
  }
}
