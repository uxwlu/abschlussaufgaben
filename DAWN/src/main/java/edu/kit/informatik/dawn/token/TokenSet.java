package edu.kit.informatik.dawn.token;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A set of tokens.
 *
 * @param <T> the type of the tokens
 * @author uxwlu
 * @version 1.0
 */
public class TokenSet<T extends Token> {

  private final List<T> all;
  private List<T> remaining;

  /**
   * Creates a new token set.
   *
   * @param tokens the tokens in it
   */
  public TokenSet(List<T> tokens) {
    this.all = new ArrayList<>(tokens);
    this.remaining = new ArrayList<>(tokens);
  }

  /**
   * Returns all tokens.
   *
   * @return all tokens
   */
  public List<T> getAll() {
    return Collections.unmodifiableList(all);
  }

  /**
   * Returns all remaining tokens.
   *
   * @return all remaining tokens
   */
  public List<T> getRemaining() {
    return Collections.unmodifiableList(remaining);
  }

  /**
   * Returns whether there are no more tokens remaining.
   *
   * @return true if there are no remaining tokens
   */
  public boolean isEmpty() {
    return getRemaining().isEmpty();
  }

  /**
   * Indicates a token was placed.
   *
   * @param token the token that was placed
   */
  public void tokenPlaced(T token) {
    remaining.remove(token);
  }

  /**
   * Refills the set.
   */
  public void refill() {
    remaining = new ArrayList<>(all);
  }
}
