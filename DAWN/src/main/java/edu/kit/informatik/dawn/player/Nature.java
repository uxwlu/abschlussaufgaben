package edu.kit.informatik.dawn.player;

import edu.kit.informatik.dawn.token.NatureToken;
import edu.kit.informatik.dawn.token.TokenSet;
import java.util.List;

/**
 * The nature player.
 *
 * @author uxwlu
 * @version 1.0
 */
public class Nature implements Player<NatureToken> {

  private final TokenSet<NatureToken> tokens;

  /**
   * Creates a new player.
   *
   * @param tokens the tokens
   */
  public Nature(List<NatureToken> tokens) {
    this.tokens = new TokenSet<>(tokens);
  }

  @Override
  public TokenSet<NatureToken> getTokens() {
    return tokens;
  }

  /**
   * Returns the current token. Only valid if there actually is one, i.e. {@link TokenSet#isEmpty()}
   * is false.
   *
   * @return the remaining token
   */
  public NatureToken getCurrentToken() {
    return tokens.getRemaining().get(0);
  }

  @Override
  public void reset() {
    tokens.refill();

    // detach tokens
    for (NatureToken natureToken : tokens.getAll()) {
      natureToken.setPosition(null);
    }
  }
}
