package edu.kit.informatik.dawn.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * An immutable rectangle.
 *
 * <p>You can create instances using {@link #spanning(Point, Point)}.</p>
 *
 * @author uxwlu
 * @version 1.0
 */
public final class Rectangle {

  private final Point topLeft;
  private final Point bottomRight;

  /**
   * Creates a new rectangle.
   *
   * @param topLeft the coordinates of the top left point
   * @param bottomRight the coordinates of the bottom right point
   */
  private Rectangle(Point topLeft, Point bottomRight) {
    this.topLeft = topLeft;
    this.bottomRight = bottomRight;
  }

  /**
   * Returns the coordinates of the top left point.
   *
   * @return the coordinates of the top left point
   */
  public Point getTopLeft() {
    return topLeft;
  }

  /**
   * Returns the coordinates of the bottom right point.
   *
   * @return the coordinates of the bottom right point
   */
  public Point getBottomRight() {
    return bottomRight;
  }

  /**
   * Returns the width of this rectangle.
   *
   * @return the width of this rectangle
   */
  public int getWidth() {
    return bottomRight.getX() - topLeft.getX() + 1;
  }

  /**
   * Returns the height of this rectangle.
   *
   * @return the height of this rectangle
   */
  public int getHeight() {
    return bottomRight.getY() - topLeft.getY() + 1;
  }

  /**
   * Returns all points that lie within this rectangle.
   *
   * They will be returned line by line.
   *
   * @return all points that lie within this rectangle
   */
  public List<Point> getPoints() {
    List<Point> points = new ArrayList<>();

    for (int y = 0; y < getHeight(); y++) {
      for (int x = 0; x < getWidth(); x++) {
        points.add(topLeft.add(x, y));
      }
    }

    return points;
  }

  /**
   * Validates that a given point is inside the rectangle.
   *
   * @param point the point
   * @return true if the point is inside the rectangle
   */
  public boolean isInside(Point point) {
    return point.getX() >= topLeft.getX()
        && point.getY() >= topLeft.getY()
        && point.getX() <= bottomRight.getX()
        && point.getY() <= bottomRight.getY();
  }

  /**
   * Creates a Rectangle that spans the area between the two given points (inclusive).
   *
   * @param one the first point
   * @param two the second point
   * @return a Rectangle that spans the area between the two given points (inclusive)
   */
  public static Rectangle spanning(Point one, Point two) {
    return new Rectangle(
        new Point(Math.min(one.getX(), two.getX()), Math.min(one.getY(), two.getY())),
        new Point(Math.max(one.getX(), two.getX()), Math.max(one.getY(), two.getY()))
    );
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Rectangle rectangle = (Rectangle) o;
    return Objects.equals(topLeft, rectangle.topLeft)
        && Objects.equals(bottomRight, rectangle.bottomRight);
  }

  @Override
  public int hashCode() {
    return Objects.hash(topLeft, bottomRight);
  }

  @Override
  public String toString() {
    return "[" + topLeft + "->" + bottomRight + "]";
  }
}
