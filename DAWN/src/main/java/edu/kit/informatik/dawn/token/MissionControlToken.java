package edu.kit.informatik.dawn.token;

import edu.kit.informatik.commandsystem.util.Function;

/**
 * A token owned by the mission control player.
 *
 * @author uxwlu
 * @version 1.0
 */
public class MissionControlToken extends Token {

  private final MissionControlTokenType type;

  /**
   * Creates a new token.
   *
   * @param length the length of the token
   * @param tokenType the type of the token
   */
  public MissionControlToken(int length, MissionControlTokenType tokenType) {
    super(length);
    this.type = tokenType;
  }

  /**
   * Returns the token type.
   *
   * @return the token type
   */
  public MissionControlTokenType getType() {
    return type;
  }

  /**
   * Returns the symbol for this token.
   *
   * @return the symbol for this token
   */
  public String asString() {
    return getType().asString(this);
  }

  @Override
  public String abbreviate() {
    return "+";
  }

  /**
   * The type of the token.
   */
  public enum MissionControlTokenType {
    /**
     * The DAWN.
     */
    DAWN(token -> "DAWN"),
    /**
     * A normal token.
     */
    NORMAL(token -> Integer.toString(token.getLength()));

    private final Function<Token, String> toString;

    /**
     * Creates a new token type.
     *
     * @param toString a function turning tokens of that type to strings
     */
    MissionControlTokenType(Function<Token, String> toString) {
      this.toString = toString;
    }

    /**
     * Converts a token to its string representation.
     *
     * @param token the token
     * @return the token as a string
     */
    private String asString(Token token) {
      return toString.apply(token);
    }
  }

}
