package edu.kit.informatik.dawn.game;

import edu.kit.informatik.commandsystem.tree.CommandNode;
import edu.kit.informatik.commandsystem.util.Supplier;
import edu.kit.informatik.dawn.board.Board;
import edu.kit.informatik.dawn.command.CommandCollection;
import edu.kit.informatik.dawn.player.MissionControl;
import edu.kit.informatik.dawn.player.Nature;
import edu.kit.informatik.dawn.rules.Ruleset;
import edu.kit.informatik.dawn.util.Die;

/**
 * A builder for the {@link Game}.
 *
 * @author uxwlu
 * @version 1.0
 */
public final class GameBuilder {

  private final CommandCollection commands;
  private Supplier<Phase> initialPhase;
  private Board gameBoard;
  private final Ruleset rules;
  private Nature nature;
  private MissionControl missionControl;
  private Die<Integer> die;

  /**
   * Creates a new game builder.
   */
  GameBuilder() {
    this.commands = new CommandCollection();
    this.rules = new Ruleset();
  }

  /**
   * Sets the initial phase supplier.
   *
   * @param initialPhase the initial phase supplier
   * @return this builder
   */
  public GameBuilder withInitialPhase(Supplier<Phase> initialPhase) {
    this.initialPhase = initialPhase;
    return this;
  }

  /**
   * Sets the board.
   *
   * @param board the board
   * @return this builder
   */
  public GameBuilder withBoard(Board board) {
    this.gameBoard = board;
    return this;
  }

  /**
   * Sets nature.
   *
   * @param nature the nature player
   * @return this builder
   */
  public GameBuilder withNature(Nature nature) {
    this.nature = nature;
    return this;
  }

  /**
   * Sets mission control.
   *
   * @param missionControl the mission control player
   * @return this builder
   */
  public GameBuilder withMissionControl(MissionControl missionControl) {
    this.missionControl = missionControl;
    return this;
  }

  /**
   * Adds a game command. These are active for every phase.
   *
   * @param command the command
   * @return this builder
   */
  public GameBuilder withGameCommand(CommandNode<Game> command) {
    this.commands.addGameCommand(command);
    return this;
  }

  /**
   * Adds a global command.
   *
   * @param phase the phase
   * @param command the command
   * @return this builder
   */
  public GameBuilder withPhaseCommand(PhaseType phase, CommandNode<Game> command) {
    this.commands.addPhaseboundCommand(phase, command);
    return this;
  }

  /**
   * Sets the die.
   *
   * @param die the die
   * @return this builder
   */
  public GameBuilder withDie(Die<Integer> die) {
    this.die = die;
    return this;
  }


  /**
   * Builds the game.
   *
   * @return the created game
   */
  public Game build() {
    return new Game(initialPhase, gameBoard, commands, rules, nature, missionControl, die);
  }
}
