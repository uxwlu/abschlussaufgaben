package edu.kit.informatik.dawn.game.phases.naturemove;

import edu.kit.informatik.commandsystem.util.Optional;
import edu.kit.informatik.dawn.board.Board;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.moves.NatureMove;
import edu.kit.informatik.dawn.rules.MoveRule;
import edu.kit.informatik.dawn.token.Token;
import edu.kit.informatik.dawn.util.Point;
import edu.kit.informatik.dawn.util.Predicate;
import edu.kit.informatik.dawn.util.StreamSupplement;
import java.util.List;

/**
 * Ensures the path is free.
 *
 * @author uxwlu
 * @version 1.0
 */
class PathIsFreeRule implements MoveRule<NatureMove> {

  @Override
  public Class<NatureMove> getMoveClass() {
    return NatureMove.class;
  }

  @Override
  public Optional<String> validate(NatureMove move, Game game) {
    if (move.isEmpty()) {
      return Optional.empty();
    }
    Board board = game.getGameBoard();

    List<Point> path = move.getPath().subList(1, move.getPath().size());

    if (!StreamSupplement.allMatch(path, isMovable(move.getToken(), board))) {
      return Optional.of("the path is blocked!");
    }

    return Optional.empty();
  }

  /**
   * Returns a predicate checking whether the given token can move to a point.
   *
   * <p>A point is considered movable if it is free or the token is the one that is trying to
   * move.</p>
   *
   * @param token the token that moves
   * @param board the board
   * @return a predicate checking whether the given token can move to a point.
   */
  private Predicate<Point> isMovable(Token token, Board board) {
    return point -> board.get(point)
        .map(placed -> placed.equals(token))
        .orElse(true);
  }
}
