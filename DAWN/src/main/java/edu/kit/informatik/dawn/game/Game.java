package edu.kit.informatik.dawn.game;

import edu.kit.informatik.commandsystem.util.Optional;
import edu.kit.informatik.commandsystem.util.Supplier;
import edu.kit.informatik.dawn.board.Board;
import edu.kit.informatik.dawn.command.CommandCollection;
import edu.kit.informatik.dawn.moves.Move;
import edu.kit.informatik.dawn.player.MissionControl;
import edu.kit.informatik.dawn.player.Nature;
import edu.kit.informatik.dawn.rules.MoveRule;
import edu.kit.informatik.dawn.rules.Ruleset;
import edu.kit.informatik.dawn.util.Die;
import java.util.Objects;

/**
 * The game base class.
 *
 * <p>You can instantiate a game by using the {@link #builder()} method.</p>
 *
 * @author uxwlu
 * @version 1.0
 */
public final class Game {

  private final CommandCollection commands;
  private final Board gameBoard;
  private final Supplier<Phase> initialPhase;
  private final Ruleset rules;
  // this will never change in this game, so hardcode the two parties
  private final Nature nature;
  private final MissionControl missionControl;
  private final Die<Integer> die;

  private Phase activePhase;

  /**
   * Creates a new game.
   *
   * @param initialPhase the initial phase supplier
   * @param gameBoard the game board
   * @param commands the commands
   * @param rules the rules
   * @param nature {@link Nature}
   * @param missionControl {@link MissionControl}
   * @param die the die to use for rolling
   */
  Game(Supplier<Phase> initialPhase, Board gameBoard, CommandCollection commands,
      Ruleset rules, Nature nature, MissionControl missionControl, Die<Integer> die) {
    this.initialPhase = Objects.requireNonNull(initialPhase, "initialPhase can not be null!");
    this.gameBoard = Objects.requireNonNull(gameBoard, "gameBoard can not be null!");

    this.commands = Objects.requireNonNull(commands, "commands can not be null!");
    this.rules = Objects.requireNonNull(rules, "rules can not be null!");

    this.missionControl = Objects.requireNonNull(missionControl, "missionControl can not be null!");
    this.nature = Objects.requireNonNull(nature, "nature can not be null!");
    this.die = Objects.requireNonNull(die, "die can not be null!");

    switchPhase(initialPhase.get());
  }

  /**
   * Returns the game board.
   *
   * @return the game board
   */
  public Board getGameBoard() {
    return gameBoard;
  }

  /**
   * Returns all commands.
   *
   * @return the command collection
   */
  public CommandCollection getCommands() {
    return commands;
  }

  /**
   * Returns the rule set.
   *
   * @return the rule set
   */
  public Ruleset getRules() {
    return rules;
  }

  /**
   * Returns the nature player.
   *
   * @return the nature player
   */
  public Nature getNature() {
    return nature;
  }

  /**
   * Returns the mission control player.
   *
   * @return the mission control player
   */
  public MissionControl getMissionControl() {
    return missionControl;
  }

  /**
   * Returns the game's die.
   *
   * @return the die
   */
  public Die<Integer> getDie() {
    return die;
  }

  /**
   * Switches the current phase.
   *
   * @param newPhase the new phase
   */
  public void switchPhase(Phase newPhase) {
    Objects.requireNonNull(newPhase, "newPhase can not be null!");

    if (activePhase != null) {
      for (MoveRule<?> rule : activePhase.getRules()) {
        rules.removeRule(rule);
      }
    }

    commands.phaseSwitched(newPhase);

    for (MoveRule<?> rule : newPhase.getRules()) {
      rules.addRule(rule);
    }

    newPhase.setup(this);
    activePhase = newPhase;
  }

  /**
   * Applies a move if possible. Returns the error message otherwise.
   *
   * @param move the move to apply
   * @return the error message, if any
   */
  public Optional<String> applyMove(Move move) {
    Optional<String> checkResult = getRules().check(move, this);

    if (checkResult.isPresent()) {
      return checkResult;
    }

    move.apply(this);

    return Optional.empty();
  }

  /**
   * Resets this game.
   */
  public void reset() {
    getGameBoard().reset();
    getNature().reset();
    getMissionControl().reset();

    switchPhase(initialPhase.get());
  }

  /**
   * Creates a game builder.
   *
   * @return the builder
   */
  public static GameBuilder builder() {
    return new GameBuilder();
  }
}
