package edu.kit.informatik.dawn.game.phases.missionplace;

import edu.kit.informatik.commandsystem.util.Optional;
import edu.kit.informatik.dawn.board.Board;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.moves.MissionControlPlacementMove;
import edu.kit.informatik.dawn.rules.MoveRule;
import edu.kit.informatik.dawn.token.MissionControlToken.MissionControlTokenType;
import edu.kit.informatik.dawn.util.StreamSupplement;

/**
 * Enforces that dawn has at least one point on the board.
 *
 * @author uxwlu
 * @version 1.0
 */
class DawnHasPointOnBoardRule implements MoveRule<MissionControlPlacementMove> {

  @Override
  public Class<MissionControlPlacementMove> getMoveClass() {
    return MissionControlPlacementMove.class;
  }

  @Override
  public Optional<String> validate(MissionControlPlacementMove move, Game game) {
    if (move.getToken().getType() != MissionControlTokenType.DAWN) {
      return Optional.empty();
    }
    Board board = game.getGameBoard();
    boolean anyInside = StreamSupplement
        .anyMatch(move.getPlacementArea().getPoints(), board::isInside);

    if (!anyInside) {
      return Optional.of("no part of dawn is inside the board!");
    }

    return Optional.empty();
  }
}
