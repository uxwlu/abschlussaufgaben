package edu.kit.informatik.dawn.game.phases.natureplace;

import edu.kit.informatik.commandsystem.util.Optional;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.moves.NaturePlacementMove;
import edu.kit.informatik.dawn.rules.MoveRule;

/**
 * Enforces that the position is on the board.
 *
 * @author uxwlu
 * @version 1.0
 */
class PositionIsOnBoardRule implements MoveRule<NaturePlacementMove> {

  @Override
  public Class<NaturePlacementMove> getMoveClass() {
    return NaturePlacementMove.class;
  }

  @Override
  public Optional<String> validate(NaturePlacementMove move, Game game) {
    if (!game.getGameBoard().isInside(move.getPoint())) {
      return Optional.of("this position is not on the board");
    }
    return Optional.empty();
  }
}
