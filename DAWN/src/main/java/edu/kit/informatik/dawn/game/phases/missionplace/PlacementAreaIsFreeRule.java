package edu.kit.informatik.dawn.game.phases.missionplace;

import edu.kit.informatik.commandsystem.util.Optional;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.moves.MissionControlPlacementMove;
import edu.kit.informatik.dawn.rules.MoveRule;

/**
 * Enforces that you do not place it somewhere it isn't free.
 *
 * @author uxwlu
 * @version 1.0
 */
class PlacementAreaIsFreeRule implements MoveRule<MissionControlPlacementMove> {

  @Override
  public Class<MissionControlPlacementMove> getMoveClass() {
    return MissionControlPlacementMove.class;
  }

  @Override
  public Optional<String> validate(MissionControlPlacementMove move, Game game) {
    if (!game.getGameBoard().isFreeOrOutside(move.getPlacementArea())) {
      return Optional.of("the area you tried to place it on is not free!");
    }
    return Optional.empty();
  }
}
