package edu.kit.informatik.dawn.game.phases.natureroll;

import static edu.kit.informatik.dawn.command.types.DieRollType.dieRoll;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.commandsystem.tree.CommandNode;
import edu.kit.informatik.commandsystem.util.Optional;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.moves.NatureRollMove;
import edu.kit.informatik.dawn.util.Die;

/**
 * A command allowing nature to roll.
 *
 * @author uxwlu
 * @version 1.0
 */
public class NatureRollCommand {

  /**
   * Returns a command allowing nature to roll.
   *
   * @param die the die to use
   * @return the command
   */
  public static CommandNode<Game> getCommand(Die<Integer> die) {
    return CommandNode.<Game>builder("roll")
        .withArgument("dieRoll", dieRoll(die))
        .withCommand(commandContext -> {
          int rolled = commandContext.getArgument(Integer.class, "dieRoll");
          Game game = commandContext.getSource();

          NatureRollMove move = NatureRollMove.of(rolled, game.getMissionControl());

          Optional<String> error = game.applyMove(move);
          if (error.isPresent()) {
            Terminal.printError(error.get());
          } else {
            Terminal.printLine("OK");
          }
        })
        .build();
  }
}
