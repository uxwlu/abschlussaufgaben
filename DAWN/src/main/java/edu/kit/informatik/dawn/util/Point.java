package edu.kit.informatik.dawn.util;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * A simple immutable point.
 *
 * @author uxwlu
 * @version 1.0
 */
public final class Point {

  private final int x;
  private final int y;

  /**
   * Creates a new point.
   *
   * @param x the x coordinate
   * @param y the y coordinate
   */
  public Point(int x, int y) {
    this.x = x;
    this.y = y;
  }

  /**
   * Returns the x coordinate.
   *
   * @return the x coordinate
   */
  public int getX() {
    return x;
  }

  /**
   * Returns the y coordinate.
   *
   * @return the y coordinate
   */
  public int getY() {
    return y;
  }

  /**
   * Returns a new point that is created by adding the given x and y offset to this point.
   *
   * @param x the x offset
   * @param y the y offset
   * @return the resulting point
   */
  public Point add(int x, int y) {
    return new Point(getX() + x, getY() + y);
  }

  /**
   * Returns a new point that is created by subtracting the other point from this point.
   *
   * @param other the point to subtract
   * @return the resulting point
   */
  private Point subtract(Point other) {
    return new Point(getX() - other.getX(), getY() - other.getY());
  }

  /**
   * Computes the manhatten distance between this point and the given.
   *
   * @param other the other point
   * @return the manhatten distance
   */
  public int manhattenTo(Point other) {
    Point offset = subtract(other);
    return Math.abs(offset.getX()) + Math.abs(offset.getY());
  }

  /**
   * Returns the adjacent points, ignoring diagonals.
   *
   * @return the adjacent points
   */
  public List<Point> getAdjacent() {
    return Arrays.asList(
        add(1, 0),
        add(0, 1),
        add(-1, 0),
        add(0, -1)
    );
  }

  /**
   * Transforms this point to be relative to the input/output coordinate system.
   *
   * @return a transformed point that is relative to the input/output coordinate system
   */
  public Point toIoCoordinateSystem() {
    return new Point(getY(), getX());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Point point = (Point) o;
    return x == point.x
        && y == point.y;
  }

  @Override
  public int hashCode() {
    return Objects.hash(x, y);
  }

  @Override
  public String toString() {
    return String.format("(%d,%d)", getX(), getY());
  }
}
