package edu.kit.informatik.dawn.game.phases.end;

import edu.kit.informatik.dawn.game.PhaseType;
import edu.kit.informatik.dawn.game.phases.AbstractPhase;
import edu.kit.informatik.dawn.rules.MoveRule;
import java.util.Collections;
import java.util.List;

/**
 * The game has ended.
 *
 * @author uxwlu
 * @version 1.0
 */
public class EndPhase extends AbstractPhase {

  @Override
  protected List<MoveRule<?>> rules() {
    // this will effectively block all moves, as no move matches
    return Collections.emptyList();
  }

  @Override
  public PhaseType getType() {
    return PhaseType.GAME_ENDED;
  }
}
