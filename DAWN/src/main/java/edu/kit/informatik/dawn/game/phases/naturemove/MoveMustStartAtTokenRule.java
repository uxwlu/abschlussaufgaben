package edu.kit.informatik.dawn.game.phases.naturemove;

import edu.kit.informatik.commandsystem.util.Optional;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.moves.NatureMove;
import edu.kit.informatik.dawn.rules.MoveRule;
import edu.kit.informatik.dawn.util.Point;

/**
 * Enforces that the moves start at the token.
 *
 * @author uxwlu
 * @version 1.0
 */
public class MoveMustStartAtTokenRule implements MoveRule<NatureMove> {

  @Override
  public Class<NatureMove> getMoveClass() {
    return NatureMove.class;
  }

  @Override
  public Optional<String> validate(NatureMove move, Game game) {
    Optional<Point> tokenPosition = move.getToken().getPosition();

    if (!tokenPosition.isPresent()) {
      return Optional.of("the token is not on the board!");
    }

    if (!tokenPosition.get().equals(move.getStart())) {
      return Optional.of("you need to start moving at the token position!");
    }

    return Optional.empty();
  }
}
