package edu.kit.informatik.dawn.game.phases.missionplace;

import static edu.kit.informatik.dawn.command.types.MissionControlFieldPositionType.missionControlPosition;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.commandsystem.exceptions.CommandMiscException;
import edu.kit.informatik.commandsystem.tree.CommandNode;
import edu.kit.informatik.commandsystem.util.Optional;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.moves.MissionControlPlacementMove;
import edu.kit.informatik.dawn.token.MissionControlToken;
import edu.kit.informatik.dawn.util.Point;
import edu.kit.informatik.dawn.util.Rectangle;

/**
 * A command that allows mission control to place a token.
 *
 * @author uxwlu
 * @version 1.0
 */
public class MissionControlPlaceCommand {

  /**
   * Returns a command that allows mission control to place a token.
   *
   * @return a command that allows mission control to place a token
   */
  public static CommandNode<Game> getCommand() {
    return CommandNode.<Game>builder("place")
        .withArgument("firstEnd", missionControlPosition())
        .withArgument("secondEnd", missionControlPosition())
        .withCommand(commandContext -> {
          Game game = commandContext.getSource();

          Point firstEnd = commandContext.getArgument(Point.class, "firstEnd");
          Point secondEnd = commandContext.getArgument(Point.class, "secondEnd");
          Rectangle area = Rectangle.spanning(firstEnd, secondEnd);

          int selectedTokenLength = Math.max(area.getWidth(), area.getHeight());
          MissionControlToken tokenToPlace = getTokenWithLength(selectedTokenLength, game)
              .orElseThrow(() -> new CommandMiscException(
                  String.format("You can not place a token of length %d!",
                      selectedTokenLength
                  )
              ));

          MissionControlPlacementMove move = MissionControlPlacementMove.of(tokenToPlace, area);

          Optional<String> error = game.applyMove(move);

          if (error.isPresent()) {
            Terminal.printError(error.get());
          } else {
            Terminal.printLine("OK");
          }
        })
        .build();
  }

  private static Optional<MissionControlToken> getTokenWithLength(int length, Game game) {
    for (MissionControlToken token : game.getMissionControl().getTokens().getRemaining()) {
      if (token.getLength() == length) {
        return Optional.of(token);
      }
    }
    return Optional.empty();
  }
}
