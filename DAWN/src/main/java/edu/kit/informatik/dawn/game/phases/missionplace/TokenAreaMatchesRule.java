package edu.kit.informatik.dawn.game.phases.missionplace;

import edu.kit.informatik.commandsystem.util.Optional;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.moves.MissionControlPlacementMove;
import edu.kit.informatik.dawn.rules.MoveRule;
import edu.kit.informatik.dawn.util.Rectangle;

/**
 * Enforces that the token area matches the given placement area.
 *
 * @author uxwlu
 * @version 1.0
 */
class TokenAreaMatchesRule implements MoveRule<MissionControlPlacementMove> {

  @Override
  public Class<MissionControlPlacementMove> getMoveClass() {
    return MissionControlPlacementMove.class;
  }

  @Override
  public Optional<String> validate(MissionControlPlacementMove move, Game game) {
    Rectangle area = move.getPlacementArea();
    int length = Math.max(area.getWidth(), area.getHeight());

    if (length != move.getToken().getLength()) {
      return Optional.of("the length does not match up!");
    }

    return Optional.empty();
  }
}
