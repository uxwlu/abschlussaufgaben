package edu.kit.informatik.dawn.game.phases.missionplace;

import edu.kit.informatik.dawn.game.PhaseType;
import edu.kit.informatik.dawn.game.phases.AbstractPhase;
import edu.kit.informatik.dawn.rules.MoveRule;
import edu.kit.informatik.dawn.token.Token;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A phase that allows mission control to place a token.
 *
 * @author uxwlu
 * @version 1.0
 */
public class MissionControlPlacesTokenPhase extends AbstractPhase {

  private final List<? extends Token> possibleTokens;

  /**
   * Creates a new phase.
   *
   * @param possibleTokens a list of tokens mission control may place down
   */
  public MissionControlPlacesTokenPhase(List<? extends Token> possibleTokens) {
    this.possibleTokens = new ArrayList<>(possibleTokens);
  }

  @Override
  public PhaseType getType() {
    return PhaseType.PLACING_MISSION_CONTROL;
  }

  @Override
  protected List<MoveRule<?>> rules() {
    return Arrays.asList(
        new PickedFromPossibleTokensRule(possibleTokens),
        new DawnHasPointOnBoardRule(),
        new PlaceNormalTokensInsideBoundsRule(),
        new PlacementAreaIsFreeRule(),
        new TokenAreaMatchesRule(),
        new TokenIsNotDiagonalRule()
    );
  }
}
