package edu.kit.informatik.dawn.util;

import edu.kit.informatik.commandsystem.util.Optional;
import edu.kit.informatik.dawn.token.MissionControlToken;
import edu.kit.informatik.dawn.token.Token;
import java.util.ArrayList;
import java.util.List;

/**
 * A die only allowing inputs matching {@link MissionControlToken#asString()}.
 *
 * @author uxwlu
 * @version 1.0
 */
public class TokenDie implements Die<Integer> {

  private final List<MissionControlToken> tokens;

  /**
   * Creates a new token die.
   *
   * @param tokens the tokens
   */
  public TokenDie(List<MissionControlToken> tokens) {
    this.tokens = new ArrayList<>(tokens);
  }

  @Override
  public Optional<Integer> roll(String value) {
    return StreamSupplement
        .first(tokens, token -> token.asString().equals(value))
        .map(Token::getLength);
  }

  @Override
  public boolean containsValue(Integer value) {
    return StreamSupplement.anyMatch(tokens, token -> token.getLength() == value);
  }
}
