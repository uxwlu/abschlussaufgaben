package edu.kit.informatik.dawn.command.commands;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.commandsystem.tree.CommandNode;
import edu.kit.informatik.commandsystem.util.Function;
import edu.kit.informatik.dawn.board.Board;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.token.Token;
import edu.kit.informatik.dawn.util.Point;
import edu.kit.informatik.dawn.util.StreamSupplement;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * A command printing the game board.
 *
 * @author uxwlu
 * @version 1.0
 */
public class PrintCommand {

  // I made a small ANSI renderer to be able to actually read the output, so this is quite generic
  // The print and state do not share a single renderer, as the ANSI renderer is not needed for
  // printing a single char.
  // The ANSI renderer is not included in the submission, I just wanted to explain the reasoning
  // for this weirdly generic and (at first glance) duplicated statement
  private static final Function<Token, String> RENDERER = new DefaultTokenRenderer();

  /**
   * Returns a command printing the board.
   *
   * @return the command
   */
  public static CommandNode<Game> getCommand() {
    return CommandNode.<Game>builder("print")
        .withCommand(commandContext -> {
          Game game = commandContext.getSource();
          Board board = game.getGameBoard();

          SortedMap<Integer, List<Point>> lines = StreamSupplement.groupingBy(
              board.getBounds().getPoints(),
              Point::getY,
              TreeMap::new
          );

          // lines is sorted, so this prints them in order
          for (List<Point> line : lines.values()) {
            List<String> renderedList = StreamSupplement.map(
                line,
                point -> RENDERER.apply(board.get(point).orElse(null))
            );
            Terminal.printLine(String.join("", renderedList));
          }
        })
        .build();
  }
}
