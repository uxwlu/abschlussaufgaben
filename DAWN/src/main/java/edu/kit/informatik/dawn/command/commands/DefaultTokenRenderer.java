package edu.kit.informatik.dawn.command.commands;

import edu.kit.informatik.commandsystem.util.Function;
import edu.kit.informatik.dawn.token.Token;

/**
 * A renderer for {@link Token}s.
 *
 * @author uxwlu
 * @version 1.0
 */
class DefaultTokenRenderer implements Function<Token, String> {

  /**
   * Renders a single token.
   *
   * Accepts {@code null} to indicate the absence of a token.
   *
   * @param token the token to render
   * @return the rendered representation
   */
  @Override
  public String apply(Token token) {
    if (token == null) {
      return "-";
    }
    return token.abbreviate();
  }
}
