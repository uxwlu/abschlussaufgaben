package edu.kit.informatik.dawn.command.commands;

import edu.kit.informatik.commandsystem.command.CommandResult;
import edu.kit.informatik.commandsystem.tree.CommandNode;
import edu.kit.informatik.dawn.game.Game;

/**
 * A command that exits the game.
 *
 * @author uxwlu
 * @version 1.0
 */
public class QuitCommand {

  /**
   * A command that exits the game.
   *
   * @return the command
   */
  public static CommandNode<Game> getCommand() {
    return CommandNode.<Game>builder("quit")
        .withCommand(commandContext -> CommandResult.EXIT)
        .build();
  }
}
