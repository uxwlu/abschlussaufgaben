package edu.kit.informatik.dawn.game;

import edu.kit.informatik.dawn.rules.MoveRule;
import java.util.List;

/**
 * A game phase.
 *
 * @author uxwlu
 * @version 1.0
 */
public interface Phase {

  /**
   * Runs setup logic.
   *
   * @param game the game instance you are attaching to
   */
  void setup(Game game);

  /**
   * Returns the type of this phase.
   *
   * @return the type of the phase
   */
  PhaseType getType();

  /**
   * Returns all rules this phase needs. If the rule list is empty, no move will be possible.
   *
   * @return the rules this phase needs
   */
  List<MoveRule<?>> getRules();
}
