package edu.kit.informatik.dawn.rules;

import edu.kit.informatik.commandsystem.util.Optional;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.moves.Move;

/**
 * A rule that checks whether a move is valid.
 *
 * @param <T> the type of the rule
 * @author uxwlu
 * @version 1.0
 */
// The rule system is quite verbose, making quite a few classes which all contain boilerplate code
// necessary. However, it is far more flexible than consolidating these rules in a single
// (or select few) places. One could also argue that the SRP comes into play as well, as each rule
// class only has a small, well defined responsibility: Enforce a *single* rule.
// Furthermore it allows dynamically removing and adding rules, changing them up based on outside
// conditions, time, game state or the chosen mode/board. You can also use them to implement
// variants: have you ever played a single e.g. "UNO" or "Ludo" game *without* any homebrew rules?
// The system is a bit more verbose and abstract, but it buys you a lot of flexibility into a
// direction I'd consider *really* likely to be extended on in the future.
public interface MoveRule<T extends Move> {

  /**
   * Returns the class of the move it can handle.
   *
   * @return the class of the move it can handle
   */
  // All moves are *interfaces*, so this is not quite restraining for future growth and the coupling
  // is extremely loose.
  Class<T> getMoveClass();

  /**
   * Validates a move, returning an error message if it is not valid.
   *
   * @param move the move to validate
   * @param game the game
   * @return the error message
   */
  Optional<String> validate(T move, Game game);
}
