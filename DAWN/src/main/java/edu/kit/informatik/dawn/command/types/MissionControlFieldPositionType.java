package edu.kit.informatik.dawn.command.types;

import static edu.kit.informatik.commandsystem.util.CommandInvariant.none;

import edu.kit.informatik.commandsystem.arguments.ArgumentType;
import edu.kit.informatik.commandsystem.arguments.RepeatedArgumentType;
import edu.kit.informatik.commandsystem.arguments.StatelessArgumentType;
import edu.kit.informatik.commandsystem.exceptions.CommandSyntaxException;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.util.Point;
import java.util.List;
import java.util.regex.Pattern;

/**
 * An {@link ArgumentType} for mission control token positions.
 *
 * @author uxwlu
 * @version 1.0
 */
public class MissionControlFieldPositionType {

  /**
   * Parses a position consisting entirely of positive integers.
   *
   * @return an argument type parsing a position for a nature token
   */
  public static ArgumentType<Point, Game> missionControlPosition() {
    return builder -> {
      ArgumentType<List<Integer>, Game> parser = RepeatedArgumentType
          .repeated(2, optionallyNegativeInteger(), ';', none());

      List<Integer> cordinates = parser.parse(builder);

      // The coordinate system used by the game is the opposite of ours,
      // so we need to convert it to proper x/y
      return new Point(cordinates.get(1), cordinates.get(0));
    };
  }

  private static <S> StatelessArgumentType<Integer, S> optionallyNegativeInteger() {
    return stringReader -> {
      String read = stringReader.read(Pattern.compile("(-)?\\d+"));

      try {
        int parsed = Integer.parseInt(read);

        if (read.matches("(-)?0\\d+")) {
          throw new CommandSyntaxException(
              "The integer can be negative, but still not padded",
              stringReader
          );
        }

        if (read.matches("-0+")) {
          throw new CommandSyntaxException("-0+ is not allowed", stringReader);
        }

        return parsed;
      } catch (NumberFormatException e) {
        throw new CommandSyntaxException(
            "Expected an (optionally negative) integer!",
            stringReader
        );
      }
    };
  }

}
