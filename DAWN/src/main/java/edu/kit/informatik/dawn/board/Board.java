package edu.kit.informatik.dawn.board;

import edu.kit.informatik.commandsystem.util.Optional;
import edu.kit.informatik.dawn.token.Token;
import edu.kit.informatik.dawn.util.Point;
import edu.kit.informatik.dawn.util.Rectangle;
import edu.kit.informatik.dawn.util.StreamSupplement;
import java.util.HashMap;
import java.util.Map;

/**
 * A game board storing tokens.
 *
 * @author uxwlu
 * @version 1.0
 */
public class Board {

  private final Rectangle area;
  private final Map<Point, Token> data;

  /**
   * Creates a new board.
   *
   * @param area the area of the board
   */
  public Board(Rectangle area) {
    this.area = area;
    this.data = new HashMap<>();
  }

  /**
   * Returns the board bounds.
   *
   * @return the bounds
   */
  public Rectangle getBounds() {
    return area;
  }

  /**
   * Returns the {@link Token} at the given position or empty if there is none.
   *
   * <p>Does not perform any bounds checking. Invalid coordinates will just return an empty
   * optional.</p>
   *
   * @param point the position
   * @return the token at this position
   */
  public Optional<Token> get(Point point) {
    return Optional.ofNullable(data.get(point));
  }

  /**
   * Checks whether a point is inside the board boundaries.
   *
   * @param point the point
   * @return true if the point is inside the board
   */
  public boolean isInside(Point point) {
    return getBounds().isInside(point);
  }

  /**
   * Sets the {@link Token} at the given position.
   *
   * <p>Silently ignores attempts to insert points that are not {@link #isInside(Point)
   * inside}.</p>
   *
   * @param point the position
   * @param token the token
   */
  public void set(Point point, Token token) {
    // as this board lazily creates entries, we need to do our own bounds checking
    if (!isInside(point)) {
      return;
    }
    data.put(point, token);
  }

  /**
   * Sets the {@link Token} at the given area.
   *
   * @param area the area the token spans
   * @param token the token
   */
  public void set(Rectangle area, Token token) {
    for (Point point : area.getPoints()) {
      set(point, token);
    }
  }

  /**
   * Returns whether the point has a free neighbouring cell.
   *
   * @param point the point
   * @return true if the point has a free neighbouring cell
   */
  public boolean hasFreeNeighbour(Point point) {
    return StreamSupplement.anyMatch(point.getAdjacent(), this::isFree);
  }

  /**
   * Returns whether a cell is free, i.e. it is {@link #isInside(Point) inside} and not occupied.
   *
   * @param point the position
   * @return true if the cell is free or out of bounds
   */
  public boolean isFree(Point point) {
    return !get(point).isPresent() && isInside(point);
  }

  /**
   * Returns whether a region is free or outside the board.
   *
   * @param rectangle the rectangle to check
   * @return true if the region is free or outside the board
   */
  public boolean isFreeOrOutside(Rectangle rectangle) {
    return StreamSupplement.allMatch(
        rectangle.getPoints(),
        point -> isFree(point) || !isInside(point)
    );
  }

  /**
   * Resets the board, removing all tokens.
   */
  public void reset() {
    data.clear();
  }
}
