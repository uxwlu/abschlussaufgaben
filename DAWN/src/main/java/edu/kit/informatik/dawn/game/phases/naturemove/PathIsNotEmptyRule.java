package edu.kit.informatik.dawn.game.phases.naturemove;

import edu.kit.informatik.commandsystem.util.Optional;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.moves.NatureMove;
import edu.kit.informatik.dawn.rules.MoveRule;

/**
 * Enforces that the path is not empty, if the token can move.
 *
 * @author uxwlu
 * @version 1.0
 */
class PathIsNotEmptyRule implements MoveRule<NatureMove> {

  @Override
  public Class<NatureMove> getMoveClass() {
    return NatureMove.class;
  }

  @Override
  public Optional<String> validate(NatureMove move, Game game) {
    if (!move.isEmpty()) {
      return Optional.empty();
    }

    if (game.getGameBoard().hasFreeNeighbour(move.getStart())) {
      return Optional.of("you need to move!");
    }

    return Optional.empty();
  }
}
