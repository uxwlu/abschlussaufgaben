package edu.kit.informatik.dawn.game.phases.natureplace;

import edu.kit.informatik.commandsystem.util.Optional;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.moves.NaturePlacementMove;
import edu.kit.informatik.dawn.rules.MoveRule;
import edu.kit.informatik.dawn.token.NatureToken;

/**
 * Enforces that the correct token is moved.
 *
 * @author uxwlu
 * @version 1.0
 */
class MovesCorrectTokenRule implements MoveRule<NaturePlacementMove> {

  private final NatureToken token;

  /**
   * Creates a new rule.
   *
   * @param token the token to move
   */
  MovesCorrectTokenRule(NatureToken token) {
    this.token = token;
  }

  @Override
  public Class<NaturePlacementMove> getMoveClass() {
    return NaturePlacementMove.class;
  }

  @Override
  public Optional<String> validate(NaturePlacementMove move, Game game) {
    if (move.getToken() != token) {
      return Optional.of("you are not moving the correct token");
    }
    return Optional.empty();
  }
}
