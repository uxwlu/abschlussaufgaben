package edu.kit.informatik.dawn.game.phases.missionplace;

import edu.kit.informatik.commandsystem.util.Optional;
import edu.kit.informatik.dawn.board.Board;
import edu.kit.informatik.dawn.game.Game;
import edu.kit.informatik.dawn.moves.MissionControlPlacementMove;
import edu.kit.informatik.dawn.rules.MoveRule;
import edu.kit.informatik.dawn.token.MissionControlToken.MissionControlTokenType;
import edu.kit.informatik.dawn.util.StreamSupplement;

/**
 * Enforces that normal tokens are placed inside the board.
 *
 * @author uxwlu
 * @version 1.0
 */
class PlaceNormalTokensInsideBoundsRule implements MoveRule<MissionControlPlacementMove> {

  @Override
  public Class<MissionControlPlacementMove> getMoveClass() {
    return MissionControlPlacementMove.class;
  }

  @Override
  public Optional<String> validate(MissionControlPlacementMove move, Game game) {
    if (move.getToken().getType() != MissionControlTokenType.NORMAL) {
      return Optional.empty();
    }
    Board board = game.getGameBoard();
    boolean inside = StreamSupplement
        .allMatch(move.getPlacementArea().getPoints(), board::isInside);

    if (!inside) {
      return Optional.of("not all points are within the board area!");
    }

    return Optional.empty();
  }
}
